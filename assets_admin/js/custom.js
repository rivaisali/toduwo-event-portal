$("#cancelCreateUndangan").on('click', function () {
	window.location.href = url + '/login/logout';
})

$("#subdomain").on('input', function () {
	let subDomain = $(this).val();
	const cekDomain = $(".cekDomain");
	if (subDomain.length > 0) {
		cekDomain.show();
	} else {
		cekDomain.hide();
	}
	cekDomain.html(``);
	cekDomain.html(`<span class="text-primary">Mengecek Sub Domain</span>`);
})

// konvert ke rupiah
function convertToRupiah(angka) {
	var rupiah = '';
	var angkarev = angka.toString().split('').reverse().join('');
	for (var i = 0; i < angkarev.length; i++)
		if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
	return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
}

// show password
$(".toggle-password").click(function () {
	$(this).find(".mdi").toggleClass("mdi-eye mdi-eye-off");
	var input = $($(this).attr("toggle"));
	if (input.attr("type") == "password") {
		input.attr("type", "text");
	} else {
		input.attr("type", "password");
	}
});

// clear modal
function kosongkanModal() {
	const title = $(".modal-content").find(".modal-title");
	const body = $(".modal-content").find(".modal-body");

	title.html("");
	body.html("");
}

// konfirmasi delete
// $(".delete").on("click", function(e){
$(document).on('click','.delete', function(e){
	e.preventDefault();
	const link = $(this).attr("href");
	Swal.fire({
		title: 'Apakah anda yakin?',
		text: "Data akan dihapus dari aplikasi",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ya, Hapus!',
		cancelButtonText: 'Batal'
	  }).then((result) => {
		if (result.value) {
			document.location.href = link;
		}
	  })
})

// konfirmasi reset password
// $(".reset").on("click", function(e){
$(document).on('click','.reset', function(e){
	e.preventDefault();
	const link = $(this).attr("href");
	Swal.fire({
		title: 'Apakah anda yakin?',
		text: "Password akan direset menjadi 12345678",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ya, Reset!',
		cancelButtonText: 'Batal'
	  }).then((result) => {
		if (result.value) {
			document.location.href = link;
		}
	  })
})

$("#checkAll").click(function(){
    $('input:checkbox').not(this).not(":disabled").prop('checked', this.checked);
});

$( "#no_telp_cari" ).autocomplete({
	source: function( request, response ) {
		const undanganId = $("#undangan_id").val();
		// Fetch data
		$.ajax({
		url: url+"undangan/get_autocomplete/"+request.term+"/"+undanganId,
		type: 'post',
		dataType: "json",
		success: function( data ) {
			response( data );
		}
	});
	},
	select: function (event, ui) {
		// Set selection
		$('#nama').val(ui.item.nama); // display the selected text
		$('#no_telp_cari').val(ui.item.no_telp); // save selected id to input
		$('#alamat').val(ui.item.alamat); // save selected id to input
		$('#email').val(ui.item.email); // save selected id to input
		$('#instansi').val(ui.item.instansi); // save selected id to input
		$('#jabatan').val(ui.item.jabatan); // save selected id to input
		return false;
	},
	minLength: 1
});

// $(".kirimUndanganAll").on("click",function(e){
$(document).on('click','.kirimUndanganAll', function(e){
	e.preventDefault();
	$('#responseDiv').html("");
	$('#responseDiv').hide();
	const link = $(this).attr("href");
	$.ajax({
		url: link,
		type: 'post',
		dataType: "json",
		beforeSend: function(){
			$(".kirimUndanganAll").html("<span class='mdi mdi-spinner mdi-hc-spin'></span> Mengirim Pesan");
		},
		success: function( response ) {
			if(response.error){
				Swal.fire({
					position:"top-end",
					icon: 'error',
					title:response.message,
					showConfirmButton:!1,
					timer:1500
				});
			}else{
				Swal.fire({
					position:"top-end",
					icon: 'success',
					title:"Undangan Terkirim",
					showConfirmButton:!1,
					timer:1500
				});
			}
			// if(response.error){
			// 	$('#responseDiv').removeClass('alert alert-success').addClass('alert alert-danger').show();
			// 	$('#responseDiv').html(response.message);
			// }else{
			// 	$('#responseDiv').removeClass('alert alert-danger').addClass('alert alert-success').show();
			// 	$('#responseDiv').html(response.message);
			// }
			$(".kirimUndanganAll").html("<span class='mdi mdi-mail-send'></span> Kirim Undangan");
		}
	});
})

// $(".kirimUndangan").on("click",function(e){
	$(document).on('click','.kirimUndangan', function(e){
	e.preventDefault();
	// alert('oke');
	const el = $(this);
	const link = $(this).attr("href");
	$.ajax({
		url: link,
		type: 'post',
		dataType: "json",
		beforeSend: function(){
			$(el).html("<span class='mdi mdi-spinner mdi-hc-spin text-success'></span>");
		},
		success: function( response ) {
			if(response.error){
				Swal.fire({
					position:"top-end",
					icon: 'error',
					title:response.message,
					showConfirmButton:!1,
					timer:1500
				});
			}else{
				Swal.fire({
					position:"top-end",
					icon: 'success',
					title:"Undangan Terkirim",
					showConfirmButton:!1,
					timer:1500
				});
			}
			$(el).html("<span class='mdi mdi-mail-send text-success'></span>");
		}
	});
})

$("#cetakDaftarHadir").on("click", function(){
	const undangan_id = $("#undangan_id").val();
	const kategori = $("#kategori").val();
	$.ajax({
		url: url+'/cetak_absen',
		data: {
			'undangan_id' : undangan_id,
			'kategori' : kategori
		},
		type: 'post',
		dataType: "html",
		success: function( response ) {
			console.log(response);
		}
	});
})

// preview Gambar Broadcast
function previewGambar() {
	const foto = document.querySelector("#gambar");
	// const fotoLabel = document.querySelector('.ktp-label');
	const ktpPreview = document.querySelector('.gambar-preview');

	// fotoLabel.textContent = foto.files[0].name;

	const fileFoto = new FileReader();
	fileFoto.readAsDataURL(foto.files[0]);

	fileFoto.onload = function (e) {
		ktpPreview.src = e.target.result;
	}
}

// autocomplete paket pertanyaan absen
// $( "#paket" ).autocomplete({
// 	source: url + '/pertanyaan/source'
// });

$( "#paket" ).autocomplete({
	source: function( request, response ) {
		// Fetch data
		$.ajax({
		url: url+"pertanyaan/source/"+request.term,
		// url: url+"pertanyaan/source/",
		type: 'post',
		// data: {
		// 	'term' : request.term
		// },
		dataType: "json",
		// data: {
			// search: request.term
			// undanganId = undanganId
		// },
		success: function( data ) {
			response( data );
		}
		});
	},
	select: function (event, ui) {
		// Set selection
		$('#paket').val(ui.item.paket); // display the selected text
		// $('#no_telp_cari').val(ui.item.no_telp); // save selected id to input
		// $('#alamat').val(ui.item.alamat); // save selected id to input
		// $('#email').val(ui.item.email); // save selected id to input
		// $('#instansi').val(ui.item.instansi); // save selected id to input
		// $('#jabatan').val(ui.item.jabatan); // save selected id to input
		return false;
	},
	minLength: 1
});


// kirim pesan broadcast dari detail undangan
// $('form#formBroadcast').submit(function(e) {
	$(document).on('submit','form#formBroadcast', function(e){

    const form = $(this);

    e.preventDefault();
	const cekTamu = validate();
	if(cekTamu){
		const action = $('form#formBroadcast').attr('action');
		const el = $(".kirimBC");
		
		// console.log(form);

		$.ajax({
			type: "POST",
			url: action,
			data: form.serialize(), // <--- THIS IS THE CHANGE
			dataType: "json",
			beforeSend: function(){
				$(el).html("<span class='mdi mdi-spinner mdi-hc-spin'></span>");
			},
			success: function(response){
				// console.log(response);
				if(response.error){
					Swal.fire({
						position:"top-end",
						icon: 'error',
						title:response.message,
						showConfirmButton:!1,
						timer:1500
					});
				}else{
					$('input:checkbox').not(this).not(":disabled").prop('checked', false);
					Swal.fire({
						position:"top-end",
						icon: 'success',
						title:response.message,
						showConfirmButton:!1,
						timer:1500
					});
				}
				$(el).html(`<span class="icon icon-left mdi mdi-whatsapp text-white"></span> Kirim`);
			}
		});
	}else{
		console.log('error');
	}

});

function validate()
{
	var checked=false;
    var elements = document.getElementsByClassName("pilihTamuBroadcast");
    for(var i=0; i < elements.length; i++){
        if(elements[i].checked) {
            checked = true;
        }
    }
    if (!checked) {
        alert('Pilih Salah Satu Tamu');
    }
    return checked;
}