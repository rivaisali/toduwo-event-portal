$('.select2').select2({
    theme: 'bootstrap4',
});

$("#propinsi").on("change", function() {
    const propinsi = $(this).val();
    $("#kota").empty().trigger('change.select2')
    // $("#kecamatan").empty().trigger('change.select2')
    // $("#kelurahan").empty().trigger('change.select2')
    // $('#kota').select2("val", null);
    // $("#kecamatan").empty().trigger('change')

    let data = $.ajax({
        type: 'POST',
        url: url + 'cari_kota',
        async: false,
        dataType: 'json',
        data: {
            propinsi: propinsi,
        },
        done: function(results) {
            // Uhm, maybe I don't even need this?
            JSON.parse(results);
            return results;
        },
        fail: function(jqXHR, textStatus, errorThrown) {
            console.log('Could not get posts, server response: ' + textStatus + ': ' + errorThrown);
        }
    }).responseJSON; // <-- this instead of .responseText

    // console.log(data);

    var newOption = new Option("- Pilih - ", "", false, false);
    $('#kota').append(newOption).trigger('change.select2');
    data.data.forEach(e => {
        var newOption = new Option(e.kodekabkota + ' ' + e.namakabkota, e.kodekabkota + ' ' + e.namakabkota, false, false);
        $('#kota').append(newOption).trigger('change.select2');
    });
})

$("#kota").on("change", function() {
    const kota = $(this).val();
    $("#kecamatan").empty().trigger('change.select2')
    $("#kelurahan").empty().trigger('change.select2')
    console.log(kota);

    let data = $.ajax({
        type: 'POST',
        url: url + 'cari_kecamatan',
        async: false,
        dataType: 'json',
        data: {
            kota: kota,
        },
        done: function(results) {
            // Uhm, maybe I don't even need this?
            JSON.parse(results);
            return results;
        },
        fail: function(jqXHR, textStatus, errorThrown) {
            console.log('Could not get posts, server response: ' + textStatus + ': ' + errorThrown);
        }
    }).responseJSON; // <-- this instead of .responseText

    // console.log(data);

    var newOption = new Option("- Pilih - ", "", false, false);
    $('#kecamatan').append(newOption).trigger('change.select2');
    data.data.forEach(e => {
        var newOption = new Option(e.namakecamatan, e.namakecamatan, false, false);
        $('#kecamatan').append(newOption).trigger('change.select2');
    });
})

$("#kecamatan").on("change", function() {
    const kecamatan = $(this).val();
    $("#kelurahan").empty().trigger('change.select2')
    // console.log(kota);

    let data = $.ajax({
        type: 'POST',
        url: url + 'cari_kelurahan',
        async: false,
        dataType: 'json',
        data: {
            kecamatan: kecamatan,
        },
        done: function(results) {
            // Uhm, maybe I don't even need this?
            JSON.parse(results);
            return results;
        },
        fail: function(jqXHR, textStatus, errorThrown) {
            console.log('Could not get posts, server response: ' + textStatus + ': ' + errorThrown);
        }
    }).responseJSON; // <-- this instead of .responseText

    // console.log(data);

    var newOption = new Option("- Pilih - ", "", false, false);
    $('#kelurahan').append(newOption).trigger('change.select2');
    data.data.forEach(e => {
        var newOption = new Option(e.kelurahan, e.kelurahan, false, false);
        $('#kelurahan').append(newOption).trigger('change.select2');
    });
})

$(document).ready(function() {
    $('form').submit(function() {
        $(this).find("button[type='submit']").prop('disabled',true);
        $(this).find("button[type='submit']").html(`<i class="fas fa-spinner fa-spin"></i> Mengirim Formulir`);
      });
});

$("#absenCekKode").on("click", function(){
	const undangan_id = $("#undangan_id").val();
	const kode = $("#kode").val();
	$.ajax({
		url: url+'/cek-kode-absen',
		data: {
			'undangan_id' : undangan_id,
			'kode' : kode
		},
		type: 'post',
		dataType: "html",
        beforeSend: function() {
            // setting a timeout
            $("#box-response").html(`<div class="alert alert-info"><i class="fas fa-spinner fa-spin"></i> Mencari Data</div>`);
        },
		success: function( html ) {
			$("#box-response").html(html);
		}
	});
})