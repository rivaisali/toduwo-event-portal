<!DOCTYPE html>
<html lang="en">
<?php
$path = base_url("assets/images/sertifikat.jpg");
$type = pathinfo($path, PATHINFO_EXTENSION);
$data = file_get_contents($path);
$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

$path_logo = base_url("assets/images/event-logo.png");
$type_logo = pathinfo($path_logo, PATHINFO_EXTENSION);
$data_logo = file_get_contents($path_logo);
$base64_logo = 'data:image/' . $type . ';base64,' . base64_encode($data_logo);
?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sertifikat</title>
    <style>
        @font-face {
            font-family: 'Amagro-bold';
            src: url('<?= base_url("assets/font_sertifikat/Amagro-bold.ttf") ?>') format('truetype');
        }

        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
            background-image: url(<?= $base64; ?>);
            background-position: center;
            background-size: cover;
            position: relative;
            text-align: center;
            background-repeat: no-repeat;
            width: 100%;
            height: 100%;
        }

        img.logo {
            margin-top: 25px;
            text-align: right;
            width: 100px;
        }

        .kop {
            /* border: 1px solid red; */
            height: 111px;
        }

        .kop h1 {
            color: #03757f;
            font-size: 50px;
            padding: 60px 0 0 0;
            /* margin-top: 50px; */
            letter-spacing: 10px;
        }

        .participation {
            display: block;
            margin-top: 20px;
            color: #fff;
            font-size: 25px;
            text-align: center;
            font-weight: bold;
            letter-spacing: 10px;
        }

        .box-judul {
            /* border: 1px solid yellow; */
            display: block;
            height: 115px;
        }

        .judul {
            text-align: center;
            display: block;
            margin-top: 10px;
            color: #999999;
        }

        .tema {
            text-align: center;
            display: block;
            margin-top: 5px;
            color: #999999;
            font-weight: bold;
        }

        .box-nama {
            /* border: 1px solid black; */
            height: 75px;
        }

        .nama {
            display: block;
            color: #000;
            font-size: 45px;
            text-align: center;
            font-weight: bold;
        }

        .box-footer {
            /* border: 1px solid green; */
            height: 120px;
        }

        .footer {
            text-align: center;
            display: block;
            color: #999999;
            margin-top: 10px;
            height: 70px;
            /* background-color: red; */
        }

        .tanggal {
            text-align: center;
            display: block;
            color: #999999;
            font-size: 25px;
            font-weight: bold;
        }

        .ttd {
            display: block;
            width: 500px;
            height: 100px;
            color: #999999;
        }

        .penyelenggara {
            font-size: 15px;
            /* margin: 0 auto; */
            /* line-height: 50px; */
            display: block;
            padding-top: 50px;
            text-align: center;
        }
    </style>
</head>

<body>
    <img src="<?= $base64_logo ?>" alt="logo" class="logo">
    <div class="kop">
        <h1>SERTIFIKAT</h1>
    </div>

    <div class="box-partisipasi">
        <span class="participation">
            PESERTA
        </span>
    </div>
    <div class="box-judul">
        <span class="judul">
            a
        </span>
        <span class="tema">
            "Indonesia #MAKINCAKAPDIGITAL - DAKWAH YANG RAMAH DI INTERNET"
        </span>
    </div>
    <div class="box-nama">
        <span class="nama">
            RISKIANTO A. MAHMUD
        </span>
    </div>
    <div class="box-footer">
        <span class="footer">
            y
        </span>
        <span class="tanggal">
            01 Juli 2021
        </span>
    </div>
    <span class="ttd">
        <span class="penyelenggara">
            Diselenggarakan Oleh:
            Kominfo Kota Gorontalo
        </span>
    </span>
</body>

</html>