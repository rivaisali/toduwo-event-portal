<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?>
                <?php //if (count($data) < 3) : 
                ?>
                <div class="tools dropdown">
                    <a href="<?= base_url($base . "/tambah"); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-plus text-white"></span> Tambah Data
                    </a>
                </div>
                <?php //endif; 
                ?>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Background</th>
                            <th>Judul</th>
                            <!-- <th>Logo</th>
                            <th>Keterangan</th>
                            <th>Footer</th> -->
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data as $d) : ?>
                            <tr class="odd gradeX">
                                <td><?= $no++; ?></td>
                                <td>
                                    <img src="<?= base_url("uploads/background/" . $d->background); ?>" alt="Background" class="img-thumbnail" width="100">
                                </td>
                                <td><?= $d->judul; ?></td>
                                <!-- <td>
                                    <img src="<?= base_url("uploads/logo/" . $d->logo); ?>" alt="Logo" class="img-thumbnail" width="100">
                                </td>
                                <td><?= $d->keterangan; ?></td>
                                <td><?= $d->footer; ?></td> -->
                                <td class="actions">
                                    <a class="icon mx-1" href="<?= base_url($base . "/cek-tampilan/" . $d->format_sertifikat_id); ?>" title="Cek Tampilan" target="_blank">
                                        <i class="mdi mdi-eye text-primary"></i>
                                    </a>
                                    <a class="icon mx-1" href="<?= base_url($base . "/ubah/" . $d->format_sertifikat_id); ?>" title="Ubah">
                                        <i class="mdi mdi-edit"></i>
                                    </a>
                                    <a class="icon mx-1 delete" href="<?= base_url($base . "/hapus/" . $d->format_sertifikat_id); ?>" id="hapusData" title="Hapus">
                                        <i class="mdi mdi-delete text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>