<div class="row">
    <div class="col-lg-6">
        <div class="card card-border-color card-border-color-primary">
            <!-- <div class="card-header card-header-divider"><?= $title; ?><span class="card-subtitle"></span></div> -->
            <div class="card-body">
                <?= form_open("", ["autocomplete" => "off", "enctype" => "multipart/form-data"]); ?>
                <div class="form-group pt-1">
                    <label for="background">Background</label>
                    <input type="file" name="background" class="form-control form-control-file form-control-sm <?= form_error('background') ? 'is-invalid' : ''; ?> mb-1" id="background">
                    <?= form_error('background'); ?>
                    <span class="text-muted">Hanya JPG | Maksimal 2mb</span>
                </div>
                <div class="form-group pt-1">
                    <label for="judul">Judul</label>
                    <textarea name="judul" id="judul" rows="3" class="form-control form-control-sm <?= form_error('judul') ? 'is-invalid' : ''; ?>"><?= set_value('judul', ''); ?></textarea>
                    <?= form_error('judul'); ?>
                </div>
                <!-- <div class="form-group pt-1">
                    <label for="logo">Logo</label>
                    <input type="file" name="logo" class="form-control form-control-file form-control-sm <?= form_error('logo') ? 'is-invalid' : ''; ?> mb-1" id="logo">
                    <?= form_error('logo'); ?>
                    <span class="text-muted">Hanya PNG | Maksimal 2mb</span>
                </div>
                <div class="form-group pt-1">
                    <label for="keterangan">Keterangan</label>
                    <textarea name="keterangan" id="keterangan" rows="3" class="form-control form-control-sm <?= form_error('keterangan') ? 'is-invalid' : ''; ?>"><?= set_value('keterangan', ''); ?></textarea>
                    <?= form_error('keterangan'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="footer">Footer</label>
                    <textarea name="footer" id="footer" rows="3" class="form-control form-control-sm <?= form_error('footer') ? 'is-invalid' : ''; ?>"><?= set_value('footer', ''); ?></textarea>
                    <?= form_error('footer'); ?>
                </div> -->
                <div class="row pt-3">
                    <div class="col-sm-6">
                        <p class="text-left">
                            <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                            <a href="<?= base_url($base); ?>" class="btn btn-space btn-secondary">Cancel</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>