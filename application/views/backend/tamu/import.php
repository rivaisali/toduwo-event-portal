<div class="row">
    <div class="col-lg-6">
        <div class="card card-border-color card-border-color-primary">
            <!-- <div class="card-header card-header-divider"><?=$title;?><span class="card-subtitle"></span></div> -->
            <div class="card-body">
                <?=form_open("", ["autocomplete" => "off", "enctype" => "multipart/form-data"]);?>
                <div class="form-group pt-1">
                    <label for="file_excel">File Excel</label>
                    <input type="file" name="fileURL" class="form-control form-control-file form-control-sm <?=form_error('fileURL') ? 'is-invalid' : '';?> mb-1" id="fileURL">
                    <?=form_error('fileURL');?>
                </div>
 <a href="<?=base_url()?>uploads/contoh-import-tamu.xlsx" class="badge badge-success"><span class="icon icon-left mdi mdi-download text-white"></span>&nbsp;Download Contoh File Import Tamu</a>
                <div class="row pt-3">
                    <div class="col-sm-6">
                        <p class="text-left">
                            <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                            <a href="<?=base_url($base);?>" class="btn btn-space btn-secondary">Cancel</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>