<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?>
                <?php if (count($data) < 3) : ?>
                    <div class="tools dropdown">
                        <a href="<?= base_url($base . "/tambah"); ?>" class="btn btn-space btn-primary">
                            <span class="icon icon-left mdi mdi-plus text-white"></span> Tambah Data
                        </a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Paket</th>
                            <th>Pertanyaan</th>
                            <th>Opsi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data as $d) : ?>
                            <tr class="odd gradeX">
                                <td><?= $no++; ?></td>
                                <td><?= $d->paket; ?></td>
                                <td><?= $d->pertanyaan; ?></td>
                                <td>
                                    <span class="d-block">a. <?= $d->opsi_a; ?></span>
                                    <span class="d-block">b. <?= $d->opsi_b; ?></span>
                                    <span class="d-block">c. <?= $d->opsi_c; ?></span>
                                    <span class="d-block">d. <?= $d->opsi_d; ?></span>
                                </td>
                                <td class="actions">
                                    <a class="icon mx-1" href="<?= base_url($base . "/ubah/" . $d->pertanyaan_absen_id); ?>">
                                        <i class="mdi mdi-edit"></i>
                                    </a>
                                    <a class="icon mx-1 delete" href="<?= base_url($base . "/hapus/" . $d->pertanyaan_absen_id); ?>" id="hapusData">
                                        <i class="mdi mdi-delete text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>