<div class="row">
    <div class="col-lg-12">
        <div class="card card-border-color card-border-color-primary">
            <!-- <div class="card-header card-header-divider"><?= $title; ?><span class="card-subtitle"></span></div> -->
            <div class="card-body">
                <?= form_open("", ["autocomplete" => "off", "enctype" => "multipart/form-data"]); ?>
                <?= form_hidden('id', $data->pertanyaan_absen_id); ?>
                <div class="form-group pt-1">
                    <label for="paket">Paket</label>
                    <input type="text" name="paket" id="paket" class="form-control form-control-sm <?= form_error('paket') ? 'is-invalid' : ''; ?>" value="<?= set_value('paket', $data->paket); ?>">
                    <?= form_error('paket'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="pertanyaan">Pertanyaan</label>
                    <textarea name="pertanyaan" id="pertanyaan" rows="3" class="form-control form-control-sm <?= form_error('pertanyaan') ? 'is-invalid' : ''; ?>"><?= set_value('pertanyaan', $data->pertanyaan); ?></textarea>
                    <?= form_error('pertanyaan'); ?>
                </div>
                <div class="row">
                    <div class="form-group pt-1 col-md-6">
                        <label for="opsi_a">Opsi A</label>
                        <input type="text" name="opsi_a" id="opsi_a" class="form-control form-control-sm <?= form_error('pertanyaan') ? 'is-invalid' : ''; ?>" value="<?= set_value('opsi_a', $data->opsi_a); ?>">
                        <?= form_error('opsi_a'); ?>
                    </div>
                    <div class="form-group pt-1 col-md-6">
                        <label for="opsi_b">Opsi B</label>
                        <input type="text" name="opsi_b" id="opsi_b" class="form-control form-control-sm <?= form_error('pertanyaan') ? 'is-invalid' : ''; ?>" value="<?= set_value('opsi_b', $data->opsi_b); ?>">
                        <?= form_error('opsi_b'); ?>
                    </div>
                    <div class="form-group pt-1 col-md-6">
                        <label for="opsi_c">Opsi C</label>
                        <input type="text" name="opsi_c" id="opsi_c" class="form-control form-control-sm <?= form_error('pertanyaan') ? 'is-invalid' : ''; ?>" value="<?= set_value('opsi_c', $data->opsi_c); ?>">
                        <?= form_error('opsi_c'); ?>
                    </div>
                    <div class="form-group pt-1 col-md-6">
                        <label for="opsi_d">Opsi D</label>
                        <input type="text" name="opsi_d" id="opsi_d" class="form-control form-control-sm <?= form_error('pertanyaan') ? 'is-invalid' : ''; ?>" value="<?= set_value('opsi_d', $data->opsi_d); ?>">
                        <?= form_error('opsi_d'); ?>
                    </div>
                </div>
                <div class="row pt-3">
                    <div class="col-sm-6">
                        <p class="text-left">
                            <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                            <a href="<?= base_url($base); ?>" class="btn btn-space btn-secondary">Cancel</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>