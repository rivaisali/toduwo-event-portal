<div class="be-wrapper be-fixed-sidebar">
    <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
            <div class="be-navbar-header"><a class="navbar-brand" href="<?= base_url("backend"); ?>"></a>
            </div>
            <a class="be-toggle-top-header-menu collapsed" href="#" data-toggle="collapse" aria-expanded="false" data-target="#be-navbar-collapse">Menu Cepat</a>
            <div class="navbar-collapse collapse" id="be-navbar-collapse">
                <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="<?= base_url("pengaturan/akun"); ?>">Profil</a></li>
                </ul>
            </div>
            <div class="be-right-navbar">
                <ul class="nav navbar-nav float-right be-user-nav">
                    <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false"><img src="<?= base_url(); ?>assets_admin/img/user.png" alt="Avatar"><span class="user-name"><?= user("nama"); ?></span></a>
                        <div class="dropdown-menu" role="menu">
                            <div class="user-info">
                                <div class="user-name"><?= user("nama"); ?></div>
                                <div class="user-position online">Available</div>
                            </div>
                            <!-- <a class="dropdown-item" href="<?= base_url("pengaturan/akun"); ?>"><span class="icon mdi mdi-face"></span>Akun</a> -->
                            <!-- <a class="dropdown-item" href="#"><span class="icon mdi mdi-settings"></span>Settings</a> -->
                            <a class="dropdown-item" href="<?= base_url("logout"); ?>"><span class="icon mdi mdi-power"></span>Keluar</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>