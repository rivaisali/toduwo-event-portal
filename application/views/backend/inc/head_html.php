<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?= base_url(); ?>assets_admin/img/logo-icon.png">
    <title>ngundang.in</title>
    <script>
        const url = '<?php echo base_url(); ?>';
    </script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets_admin/lib/perfect-scrollbar/css/perfect-scrollbar.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets_admin/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets_admin/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets_admin/lib/jqvmap/jqvmap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets_admin/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" /> -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets_admin/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets_admin/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets_admin/lib/bootstrap-slider/css/bootstrap-slider.min.css" />

    <!-- datatables -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets_admin/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets_admin/lib/datatables/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" />

    <!-- jquery autocomplete -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/jquery-ui.css">

    <link rel="stylesheet" href="<?= base_url(); ?>assets_admin/css/app.css" type="text/css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets_admin/css/custom.css" type="text/css" />
</head>

<body>