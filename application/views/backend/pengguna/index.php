<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?>
                <div class="tools dropdown">
                    <a href="<?= base_url($base . "/tambah"); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-plus text-white"></span> Tambah Data
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Status</th>
                            <th>Email</th>
                            <th>No. Telp</th>
                            <th>Tgl Daftar</th>
                            <th width="20">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data as $d) :
                            if ($d->status == "0") {
                                $badge  =   "danger";
                                $msg    =   "Nonaktif";
                                $label  =   "Aktifkan";
                                $icon   =   "mdi-check-circle";
                                $icon_color =   "success";
                            } else {
                                $badge  =   "success";
                                $msg    =   "Aktif";
                                $label  =   "Non-Aktifkan";
                                $icon   =   "mdi-close-circle";
                                $icon_color =   "danger";
                            }
                        ?>
                            <tr class="odd gradeX">
                                <td>
                                    <?= $d->nama; ?>
                                </td>
                                <td>
                                    <small class="badge badge-<?= $badge ?>"><?= $msg; ?></small>
                                </td>
                                <td>
                                    <?= $d->email; ?>
                                </td>
                                <td>
                                    <?= $d->no_telp; ?>
                                </td>
                                <td>
                                    <?= tgl_full($d->created_at); ?>
                                </td>
                                <td class="actions text-center">
                                    <div class="btn-group">
                                        <a title="<?= $label; ?>" class="icon mx-2" href="<?= base_url($base . "/toggle/" . $d->user_id); ?>">
                                            <i class="mdi mdi-refresh-alt text-<?= $icon_color; ?>"></i>
                                        </a>
                                        <a title="Reset Password" class="icon mx-2 reset" href="<?= base_url($base . "/reset/" . $d->user_id); ?>">
                                            <i class="mdi mdi-key text-primary"></i>
                                        </a>
                                        <a title="Ubah" class="icon mx-2" href="<?= base_url($base . "/ubah/" . $d->user_id); ?>">
                                            <i class="mdi mdi-edit"></i>
                                        </a>
                                        <a title="Hapus" class="icon mx-2 delete" href="<?= base_url($base . "/hapus/" . $d->user_id); ?>">
                                            <i class="mdi mdi-delete text-danger"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>