<div class="row">
    <div class="col-12">

        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header">
                        Data Saya
                    </div>
                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-sm-3">Nama</dt>
                            <dd class="col-sm-9"><?= $data->nama; ?></dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-3">Email</dt>
                            <dd class="col-sm-9"><?= $data->email; ?></dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-3">Telp</dt>
                            <dd class="col-sm-9"><?= $data->no_telp; ?></dd>
                        </dl>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header">
                        Ubah Password
                    </div>
                    <div class="card-body">
                        <?= form_open("", ["autocomplete" => "off"]); ?>
                        <div class="form-group m-0 mb-2">
                            <label for="password_lama">Password Lama <i class="text-danger">*</i></label>
                            <div class="input-group input-group-sm">
                                <input class="form-control form-control-sm <?= form_error('password_lama') ? 'is-invalid' : ''; ?>" name="password_lama" id="password_lama" type="password" placeholder="Password Lama" value="<?= set_value('password_lama', ''); ?>" autofocus>
                                <div class="input-group-append">
                                    <button class="btn btn-primary toggle-password" toggle="#password_lama" type="button">
                                        <span class="mdi mdi-eye"></span>
                                    </button>
                                </div>
                                <?= form_error('password_lama'); ?>
                            </div>
                        </div>
                        <div class="form-group m-0 mb-2">
                            <label for="password_baru">Password baru <i class="text-danger">*</i></label>
                            <div class="input-group input-group-sm">
                                <input class="form-control form-control-sm <?= form_error('password_baru') ? 'is-invalid' : ''; ?>" name="password_baru" id="password_baru" type="password" placeholder="Password Baru" value="<?= set_value('password_baru', ''); ?>">
                                <div class="input-group-append">
                                    <button class="btn btn-primary  toggle-password" toggle="#password_baru" type="button">
                                        <span class="mdi mdi-eye"></span>
                                    </button>
                                </div>
                                <?= form_error('password_baru'); ?>
                            </div>
                        </div>
                        <div class="form-group m-0 mb-2">
                            <label for="ulangi_password_baru">Ulangi Password baru <i class="text-danger">*</i></label>
                            <div class="input-group input-group-sm">
                                <input class="form-control form-control-sm <?= form_error('ulangi_password_baru') ? 'is-invalid' : ''; ?>" name="ulangi_password_baru" id="ulangi_password_baru" type="password" placeholder="Ulangi Password Baru" value="<?= set_value('ulangi_password_baru', ''); ?>">
                                <div class="input-group-append">
                                    <button class="btn btn-primary toggle-password" toggle="#ulangi_password_baru" type="button">
                                        <span class="mdi mdi-eye"></span>
                                    </button>
                                </div>
                                <?= form_error('ulangi_password_baru'); ?>
                            </div>
                        </div>
                        <div class="form-group pt-2">
                            <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>