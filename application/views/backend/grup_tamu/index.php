<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?>
                <div class="tools dropdown">
                    <a href="<?= base_url($base . "/tambah"); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-plus text-white"></span> Tambah Data
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Deskripsi</th>
                            <th>Jumlah Tamu</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $d) : ?>
                            <tr class="odd gradeX">
                                <td><?= $d->nama_grup_tamu; ?></td>
                                <td><?= $d->deskripsi; ?></td>
                                <td>
                                    <?= ambil_numrow_by_id_where_array("grup_tamu_detail", [
                                        "grup_tamu_id" => $d->grup_tamu_id
                                    ]); ?>
                                </td>
                                <td class="actions">
                                    <a title="Detail" class="icon mx-1" href="<?= base_url($base . "/" . $d->grup_tamu_id); ?>">
                                        <i class="mdi mdi-view-list text-primary"></i>
                                    </a>
                                    <a title="Ubah" class="icon mx-1" href="<?= base_url($base . "/ubah/" . $d->grup_tamu_id); ?>">
                                        <i class="mdi mdi-edit"></i>
                                    </a>
                                    <a title="Hapus" class="icon mx-1 delete" href="<?= base_url($base . "/hapus/" . $d->grup_tamu_id); ?>" id="hapusData">
                                        <i class="mdi mdi-delete text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>