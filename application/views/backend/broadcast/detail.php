<?php
$pesan = $data->pesan;
$pesan = str_replace('\n', "<br>", $data->pesan);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="user-info-list card">
            <div class="card-header card-header-divider">
                Detail Broadcast
                <div class="tools dropdown">
                    <a href="<?= base_url($base); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-arrow-back text-white"></span> Kembali
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <table class="no-border no-strip skills">
                            <tbody class="no-border-x no-border-y">
                                <tr>
                                    <td class="icon"><span class="mdi mdi-badge-check"></span></td>
                                    <td class="item">Judul<span class="icon s7-portfolio"></span></td>
                                    <td><?= $data->judul ?></td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-storage"></span></td>
                                    <td class="item">Pesan<span class="icon s7-gift"></span></td>
                                    <td><?= $pesan; ?></td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-calendar"></span></td>
                                    <td class="item">Waktu Kirim<span class="icon s7-gift"></span></td>
                                    <td><?= tgl_full($data->waktu_kirim); ?></td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-picture-in-picture"></span></td>
                                    <td class="item">Dengan Gambar<span class="icon s7-gift"></span></td>
                                    <td><?= ($data->gambar == "") ? 'Tidak' : 'Ya'; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php if ($data->gambar != "") : ?>
                        <div class="col-md-3">
                            <figure class="text-center">
                                <img src="<?= base_url("uploads/broadcast/" . $data->gambar); ?>" alt="Trulli" class="img-thumbnail">
                                <figcaption><?= $data->caption; ?></figcaption>
                            </figure>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">
                Daftar Penerima
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Instansi / Organisasi</th>
                            <th>Jabatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        $tamu = json_decode($data->penerima, true);
                        foreach ($tamu as $t) :
                            extract($t);
                            $data_tamu  =   ambil_data_by_id_row("tamu", "tamu_id", $tamu_id);
                        ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $data_tamu->nama_lengkap; ?></td>
                                <td><?= $data_tamu->alamat; ?></td>
                                <td><?= $data_tamu->instansi; ?></td>
                                <td><?= $data_tamu->jabatan; ?></td>
                            </tr>
                        <?php
                        endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>