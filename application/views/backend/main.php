<?php $this->load->view('backend/inc/head_html'); ?>
<?php $this->load->view('backend/inc/header'); ?>
<?php $this->load->view('backend/inc/sidebar'); ?>
<div class="be-content">
	<?php if ($this->session->flashdata("notifikasi")) { ?>
		<div class="container-fluid mt-2">
			<div class="alert alert-<?= $this->session->flashdata("notifikasi")["status"] ?> alert-dismissible" role="alert">
				<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
				<div class="icon">
					<?php if ($this->session->flashdata("notifikasi")["status"] == "success") { ?>
						<span class="mdi mdi-check"></span>
					<?php } else { ?>
						<span class="mdi mdi-close"></span>
					<?php } ?>
				</div>
				<div class="message">
					<?= $this->session->flashdata("notifikasi")["msg"]; ?>
				</div>
			</div>
		</div>
	<?php } ?>

	<div class="page-head">
		<h2 class="page-head-title mb-0"><?= ($title) ? $title : ''; ?></h2>
		<!-- <nav aria-label="breadcrumb" role="navigation">
			<ol class="breadcrumb page-head-nav">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item"><a href="#">Forms</a></li>
				<li class="breadcrumb-item active">Elements</li>
			</ol>
		</nav> -->
	</div>

	<div class="main-content container-fluid">
		<?php $this->load->view('backend/' . $page); ?>
	</div>
</div>
<?php $this->load->view('backend/inc/footer'); ?>

<div class="modal fade colored-header colored-header-primary" id="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header modal-header-colored">
				<h3 class="modal-title"></h3>
				<button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"> </span></button>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
				<button class="btn btn-secondary md-close" type="button" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('backend/inc/foot_html'); ?>