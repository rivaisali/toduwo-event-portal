<div class="row">
	<div class="col-sm-12">
		<div class="user-info-list card">
			<div class="card-header card-header-divider">
				Data Undangan
				<div class="tools dropdown">
					<a href="<?= base_url($base); ?>" class="btn btn-space btn-primary">
						<span class="icon icon-left mdi mdi-arrow-back text-white"></span> Kembali
					</a>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<table class="no-border no-strip skills">
							<tbody class="no-border-x no-border-y">
								<tr>
									<td class="icon"><span class="mdi mdi-badge-check"></span></td>
									<td class="item">Judul<span class="icon s7-portfolio"></span></td>
									<td><?= $data->judul ?></td>
								</tr>
								<tr>
									<td class="icon"><span class="mdi mdi-storage"></span></td>
									<td class="item">Keterangan<span class="icon s7-gift"></span></td>
									<td><?= $data->keterangan; ?></td>
								</tr>
								<tr>
									<td class="icon"><span class="mdi mdi-calendar-check"></span></td>
									<td class="item">Waktu<span class="icon s7-phone"></span></td>
									<td>
										<?php if ($data->tanggal == $data->tanggal_selesai) {
											echo tgl_full($data->tanggal . " " . $data->jam) . " Sampai " . jam($data->tanggal_selesai . " " . $data->jam_selesai);
										} else {
											echo tgl_full($data->tanggal . " " . $data->jam) . " Sampai " . tgl_full($data->tanggal_selesai . " " . $data->jam_selesai);
										}
										?>
									</td>
								</tr>
								<tr>
									<td class="icon"><span class="mdi mdi-home"></span></td>
									<td class="item">Lokasi<span class="icon s7-map-marker"></span></td>
									<td><?= $data->nama_lokasi; ?>
									</td>
								</tr>
								<tr>
									<td class="icon"><span class="mdi mdi-pin"></span></td>
									<td class="item">Alamat<span class="icon s7-global"></span></td>
									<td><?= $data->alamat_lokasi; ?></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-6">
						<table class="no-border no-strip skills">
							<tbody class="no-border-x no-border-y">
								<tr>
									<td class="icon"><span class="mdi mdi-accounts"></span></td>
									<td class="item">Jumlah Tamu<span class="icon s7-portfolio"></span></td>
									<td><?= count($tamu); ?></td>
								</tr>
								<tr>
									<td class="icon"><span class="mdi mdi-palette"></span></td>
									<td class="item">Format Sertifikat<span class="icon s7-portfolio"></span></td>
									<td>
										<?= ambil_nama_by_id('format_sertifikat', 'judul', 'format_sertifikat_id', $data->format_sertifikat_id); ?>
									</td>
								</tr>
								<tr>
									<td class="icon"><span class="mdi mdi-help"></span></td>
									<td class="item">Pertanyaan Absen<span class="icon s7-portfolio"></span></td>
									<td>
										<?= $data->pertanyaan_absen; ?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-12">

		<div class="card card-table">

			<div class="card-header">

				Filter Daftar Hadir
			</div>
			<div class="card-body">
				<div class="col-sm-12">
					<?= form_open("", ["autocomplete" => "off"]); ?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group pt-1">
								<label for="kategori">Kategori</label>
								<select class="form-control form-control-sm" required name="kategori" id="kategori">
									<option value="">- Pilih Kategori -</option>
									<option value="all" <?= ($filter == "all") ? 'selected' : ''; ?>>Semua</option>
									<option value="1" <?= ($filter == "1") ? 'selected' : ''; ?>>Hadir</option>
									<option value="2" <?= ($filter == "0") ? 'selected' : ''; ?>>Tidak Hadir</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group pt-1">
								<label for="tanggal">Tanggal Acara</label>
								<select class="form-control form-control-sm" required name="tanggal" id="tanggal">
									<option value="all">- Semua -</option>
									<?php
									if ($data->tanggal == $data->tanggal_selesai) {
										echo "<option value='$data->tanggal' " . ($tanggal == "all") ? 'selected' : '' . ">" . tgl_indonesia("$data->tanggal") . "</option>";
									} else {
										$dari = $data->tanggal; // tanggal mulai
										$sampai = $data->tanggal_selesai; // tanggal akhir

										while (strtotime($dari) <= strtotime($sampai)) {
									?>
											<option value="<?= $dari ?>" <?= ($tanggal == $dari) ? 'selected' : ''; ?>><?= tgl_indonesia($dari); ?></option>
									<?php
											// echo "<option value='$dari'>" . tgl_indonesia("$dari") . "</option>";
											$dari = date("Y-m-d", strtotime("+1 day", strtotime($dari)));
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>


					<div class="row pt-3">
						<div class="col-sm-6">
							<p class="text-left">
								<input type="submit" class="btn btn-space btn-primary" name="filter" value="Filter">
								<a target="_blank" href="<?= base_url("cetak_absen/$data->undangan_id/$filter/$tanggal"); ?>" class="btn btn-space btn-success">Cetak</a>
								<a target="_blank" href="<?= base_url("export_xls/$data->undangan_id/$filter/$tanggal"); ?>" class="btn btn-space btn-success">Cetak Excel</a>
								<a href="<?= base_url($base); ?>" class="btn btn-space btn-secondary">Cancel</a>
							</p>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php
	if (isset($_POST['filter'])) { ?>
		<div class="col-sm-12">

			<div class="card card-table">

				<div class="card-header">

					Daftar Hadir
				</div>
				<div class="card-body">
					<table class="table table-striped table-hover table-fw-widget" id="table4">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode Undangan</th>
								<th>Nama</th>
								<th>Alamat</th>
								<th>Status</th>
								<th>Waktu Check In</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							foreach ($tamu as $t) :
								$where_check_in	=	[
									"tamu_undangan_id"	=>	$t->tamu_undangan_id
								];
								if ($tanggal != "all") {
									$where_check_in["tanggal_check_in"]	=	$tanggal;
								}

								if ($tanggal == "all") {
									$cek_check_in	=	ambil_data_by_id_where_array("check_in", $where_check_in);
								} else {
									$cek_check_in	=	ambil_data_by_id_row_where_array("check_in", $where_check_in);
								}
							?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $t->tamu_undangan_id; ?></td>
									<td>
										<?php if ($t->tamu_id == $t->dihadiri || $t->dihadiri === null) { ?>
											<?= $t->nama_lengkap; ?>
										<?php } else { ?>
											<?= ambil_nama_by_id("tamu", "nama_lengkap", "tamu_id", $t->dihadiri); ?>
										<?php } ?>
									</td>
									<td>
										<?php if ($t->tamu_id == $t->dihadiri || $t->dihadiri === null) { ?>
											<?= $t->alamat; ?>
										<?php } else { ?>
											<?= ambil_nama_by_id("tamu", "alamat", "tamu_id", $t->dihadiri); ?>
										<?php } ?>
									</td>
									<td>
										<?php if ($t->status == 1) {
											echo "<span class='badge badge-success'>Hadir</span>";
										} else {
											echo "<span class='badge badge-danger'>Tidak Hadir</span>";
										}
										?>
									</td>
									<td>
										<?php
										if (!empty($cek_check_in)) {
											if ($tanggal == "all") {
												$no_check_in = 1;
												foreach ($cek_check_in as $ci) :
													if ($no_check_in > 1) echo "<br>";
										?>
													<?= tgl_full($ci->tanggal_check_in . " " . $ci->jam_check_in); ?>
										<?php
													$no_check_in++;
												endforeach;
											} else {
												echo tgl_full($cek_check_in->tanggal_check_in . " " . $cek_check_in->jam_check_in);
											}
										}
										?>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	<?php } ?>
</div>