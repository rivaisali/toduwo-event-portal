<div class="row">
    <div class="col-sm-12">
        <div class="user-info-list card">
            <div class="card-header card-header-divider">
                Data Undangan
                <div class="tools dropdown">
                    <a href="<?= base_url($base); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-arrow-back text-white"></span> Kembali
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <table class="no-border no-strip skills">
                            <tbody class="no-border-x no-border-y">
                                <tr>
                                    <td class="icon"><span class="mdi mdi-badge-check"></span></td>
                                    <td class="item">Judul<span class="icon s7-portfolio"></span></td>
                                    <td><?= $data->judul ?></td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-storage"></span></td>
                                    <td class="item">Keterangan<span class="icon s7-gift"></span></td>
                                    <td><?= $data->keterangan; ?></td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-calendar-check"></span></td>
                                    <td class="item">Waktu<span class="icon s7-phone"></span></td>
                                    <td>
                                        <?php if ($data->tanggal == $data->tanggal_selesai) {
                                            echo tgl_full($data->tanggal . " " . $data->jam) . " Sampai " . jam($data->tanggal_selesai . " " . $data->jam_selesai);
                                        } else {
                                            echo tgl_full($data->tanggal . " " . $data->jam) . " Sampai " . tgl_full($data->tanggal_selesai . " " . $data->jam_selesai);
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-home"></span></td>
                                    <td class="item">Lokasi<span class="icon s7-map-marker"></span></td>
                                    <td><?= $data->nama_lokasi; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-pin"></span></td>
                                    <td class="item">Alamat<span class="icon s7-global"></span></td>
                                    <td><?= $data->alamat_lokasi; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="no-border no-strip skills">
                            <tbody class="no-border-x no-border-y">
                                <tr>
                                    <td class="icon"><span class="mdi mdi-accounts"></span></td>
                                    <td class="item">Jumlah Tamu<span class="icon s7-portfolio"></span></td>
                                    <td><?= count($tamu); ?></td>
                                </tr>
                                <?php if ($data->jenis == "online") : ?>
                                    <tr>
                                        <td class="icon"><span class="mdi mdi-link"></span></td>
                                        <td class="item">
                                            Link Pendaftaran<span class="icon s7-portfolio"></span>
                                        </td>
                                        <td>
                                            <?= base_url("undangan/" . $data->undangan_id); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="icon"><span class="mdi mdi-link"></span></td>
                                        <td class="item">Link Absen<span class="icon s7-portfolio"></span></td>
                                        <td>
                                            <?= base_url("absen/" . $data->undangan_id); ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-palette"></span></td>
                                    <td class="item">Format Sertifikat<span class="icon s7-portfolio"></span></td>
                                    <td>
                                        <?= ambil_nama_by_id('format_sertifikat', 'judul', 'format_sertifikat_id', $data->format_sertifikat_id); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-help"></span></td>
                                    <td class="item">Pertanyaan Absen<span class="icon s7-portfolio"></span></td>
                                    <td>
                                        <?= $data->pertanyaan_absen; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <?php if ($tamu_belum_lihat_undangan > 0 && $data->jenis == "offline") : ?>
                            <div id="responseDiv" class="p-2"></div>
                            <a href="<?= base_url($base . "/kirim_undangan_banyak/" . $data->undangan_id); ?>" class="btn btn btn-primary kirimUndanganAll"><i class="mdi mdi-mail-send"></i> Kirim Undangan</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">
                Daftar Tamu
                <div class="tools dropdown">
                    <a href="<?= base_url("toggle-pendaftaran/" . $data->undangan_id); ?>" class="btn btn-space btn-secondary">
                        <!-- <span class="icon icon-left mdi mdi-close-box text-dark"></span> -->
                        <?= ($data->pendaftaran == "buka") ? "Tutup Pendaftaran" : "Buka Pendaftaran"; ?>
                    </a>
                    <a href="<?= base_url($base . "/broadcast/" . $data->undangan_id); ?>" class="btn btn-space btn-success">
                        <span class="icon icon-left mdi mdi-whatsapp text-white"></span>
                        Broadcast Pesan
                    </a>
                    <a href="<?= base_url($base . "/tambah_tamu/" . $data->undangan_id); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-account-add text-white"></span> Tambah Tamu
                    </a>
                    <a target="_blank" href="<?= base_url($base . "/export-peserta/" . $data->undangan_id); ?>" class="btn btn-space btn-success">
                        <span class="icon icon-left mdi mdi-download text-white"></span> Export Excel
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Kode Undangan</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <!-- <th>Lihat Undangan</th> -->
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($tamu as $t) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $t->tamu_undangan_id; ?></td>
                                <td>
                                    <?= $t->nama_lengkap; ?><br>
                                    (<?= $t->no_telp; ?>)
                                </td>
                                <td>
                                    <?= $t->alamat; ?><br>
                                    <?= $t->kelurahan . ", " . $t->kecamatan . ", " . $t->kota . ", " . $t->propinsi; ?>
                                </td>
                                <!-- <td>
                                    <span class="badge badge-<?= ($t->lihat_undangan == "1") ? "success" : "danger"; ?>">
                                        <?= ($t->lihat_undangan == "1") ? "Sudah" : "Belum"; ?>
                                    </span>
                                </td> -->
                                <td>
                                    <a title="Detail" class="icon mx-1" href="<?= base_url($base . "/detail_tamu/" . $t->tamu_undangan_id); ?>">
                                        <i class="mdi mdi-view-list text-primary"></i>
                                    </a>
                                    <?php if ($data->jenis == "offline") : ?>
                                        <a title="Hapus" class="icon mx-1 delete" href="<?= base_url($base . "/hapus_tamu/" . $t->tamu_undangan_id); ?>" id="hapusData">
                                            <i class="mdi mdi-delete text-danger"></i>
                                        </a>
                                    <?php endif; ?>
                                    <a title="Kirim Undangan" class="icon mx-1 kirimUndangan" href="<?= base_url($base . "/kirim_undangan/" . $t->tamu_undangan_id); ?>">
                                        <i class="mdi mdi-mail-send text-success"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>