<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= base_url(); ?>assets_admin/img/logo-icon.png">
    <title>Ngundang.in - Solusi Undangan Digital untuk Event</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/my.css">
    <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
    <script>
        const url = '<?php echo base_url(); ?>';
    </script>
</head>

<body>

    <div class="header-bar">
        <div class="logo">
            <img src="<?= base_url(); ?>assets/images/event-logo.png" alt="" height="60">
        </div>
        <div class="title">Absensi Peserta</div>


    </div>


    <!-- About us Start -->
    <section class="section">
        <div class="container pt-4 px-lg-5">
            <div id="about">
                <div class="row">
                    <div class="col-12 text-center">
                        <img src="<?= base_url("assets/images/logo-3.png"); ?>" width="250" alt="logo">
                        <h2 class="mb-1"><?= $undangan->judul; ?></h2>
                        <?php if ($undangan->tanggal == $undangan->tanggal_selesai) { ?>
                            <span class="event-date"><?= hari($undangan->tanggal) . ", " . tgl_indonesia($undangan->tanggal); ?></span>
                            <span class="event-date">Pukul: <?= jam($undangan->tanggal . " " . $undangan->jam) . " WIB - " . jam($undangan->tanggal_selesai . " " . $undangan->jam_selesai) . " WIB"; ?></span>
                        <?php } else { ?>
                            <span class="event-date"><?= hari($undangan->tanggal) . ", " . tgl_indonesia($undangan->tanggal); ?> - <?= hari($undangan->tanggal_selesai) . ", " . tgl_indonesia($undangan->tanggal_selesai); ?></span>
                            <span class="event-date">Pukul: <?= jam($undangan->tanggal . " " . $undangan->jam) . " WIB - " . jam($undangan->tanggal_selesai . " " . $undangan->jam_selesai) . " WIB"; ?></span>
                        <?php } ?>
                        <span class="event-date"><?= $undangan->nama_lokasi; ?></span>
                        <?= ($undangan->alamat_lokasi != "") ? "<span class='event-date'>(" . $undangan->alamat_lokasi . ")</span>" : ""; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About us End -->

    <section class="section m-0 p-2">
        <div class="container">
            <div class="px-lg-5">
                <div class="card mb-3 shadow-lg">
                    <div class="card-body">

                        <?php if ($this->session->flashdata("notifikasi")) { ?>
                            <div class="alert alert-<?= $this->session->flashdata("notifikasi")["status"] ?> alert-dismissible" role="alert">
                                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                                <div class="message">
                                    <?= $this->session->flashdata("notifikasi")["msg"]; ?>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="form-row">
                            <div class="form-group col-md-12">

                                <input type="hidden" name="undangan_id" id="undangan_id" class="form-control" value="<?= $undangan->undangan_id; ?>">
                                <input type="text" name="kode" id="kode" class="form-control form-control-lg text-center" placeholder="Masukkan Kode Registrasi">
                            </div>
                        </div>
                        <button class="btn btn-primary btn-block" id="absenCekKode">Cek Kode</button>

                        <div id="box-response" class="mt-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="<?= base_url("assets/js/my.js"); ?>"></script>
</body>

</html>
<?php //$this->load->view("frontend/inc/foot_html"); 
?>