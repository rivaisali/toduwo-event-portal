<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= base_url(); ?>assets_admin/img/logo-icon.png">
    <title>Cetak Absen</title>
    <link rel="stylesheet" href="<?= base_url("assets_admin/css/style_laporan.css"); ?>">
    <script>
        window.print();
    </script>
</head>

<body>
    <div class="kop">
        <h1>Daftar Hadir</h1>
    </div>

    <div class="judul">
        <table>
            <tr>
                <td>Kegiatan</td>
                <td>:</td>
                <td><span><?= $undangan->judul; ?></span></td>
            </tr>
            <tr>
                <td>Waktu</td>
                <td>:</td>
                <td><span>
                        <?php if ($undangan->tanggal == $undangan->tanggal_selesai) {
                            echo tgl_full($undangan->tanggal . " " . $undangan->jam) . " Sampai " . jam($undangan->tanggal_selesai . " " . $undangan->jam_selesai);
                        } else {
                            echo tgl_full($undangan->tanggal . " " . $undangan->jam) . " Sampai " . tgl_full($undangan->tanggal_selesai . " " . $undangan->jam_selesai);
                        }
                        ?>
                    </span></td>
            </tr>
            <tr>
                <td>Tempat</td>
                <td>:</td>
                <td><span><?= $undangan->nama_lokasi; ?></span></td>
            </tr>
            <tr>
                <td>Panitia</td>
                <td>:</td>
                <td><span><?= ambil_nama_by_id("user", "nama", "user_id", $undangan->user_id); ?></span></td>
            </tr>
            <tr>
                <td>Kategori</td>
                <td>:</td>
                <td>
                    <span>
                        <?php
                        if ($kategori == "all") {
                            echo "Semua";
                        } elseif ($kategori == "1") {
                            echo "Hadir";
                        } else {
                            echo "Tidak Hadir";
                        } ?>
                    </span>
                </td>
            </tr>
            <tr>
                <td>Check In</td>
                <td>:</td>
                <td>
                    <span>
                        <?php
                        if ($tanggal == "all") {
                            echo "Semua";
                        } else {
                            echo tgl_indonesia($tanggal);
                        } ?>
                    </span>
                </td>
            </tr>
        </table>
        <img src="<?= base_url("uploads/qrcode/" . $undangan->undangan_id . ".png"); ?>" alt="qr-code">
    </div>

    <table class="tamu">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Instansi / Organisasi</th>
                <th>Jabatan</th>
                <th>Ket.</th>
                <th>Waktu Check In</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1;
            foreach ($tamu as $t) :
                $where_check_in    =    [
                    "tamu_undangan_id"    =>    $t->tamu_undangan_id
                ];
                if ($tanggal != "all") {
                    $where_check_in["tanggal_check_in"]    =    $tanggal;
                }

                if ($tanggal == "all") {
                    $cek_check_in    =    ambil_data_by_id_where_array("check_in", $where_check_in);
                } else {
                    $cek_check_in    =    ambil_data_by_id_row_where_array("check_in", $where_check_in);
                }
            ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td>
                        <?php if ($t->tamu_id == $t->dihadiri || $t->dihadiri === null) { ?>
                            <?= $t->nama_lengkap; ?>
                        <?php } else { ?>
                            <?= ambil_nama_by_id("tamu", "nama_lengkap", "tamu_id", $t->dihadiri); ?>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if ($t->tamu_id == $t->dihadiri || $t->dihadiri === null) { ?>
                            <?= $t->alamat; ?>
                        <?php } else { ?>
                            <?= ambil_nama_by_id("tamu", "alamat", "tamu_id", $t->dihadiri); ?>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if ($t->tamu_id == $t->dihadiri || $t->dihadiri === null) { ?>
                            <?= $t->instansi; ?>
                        <?php } else { ?>
                            <?= ambil_nama_by_id("tamu", "instansi", "tamu_id", $t->dihadiri); ?>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if ($t->tamu_id == $t->dihadiri || $t->dihadiri === null) { ?>
                            <?= $t->jabatan; ?>
                        <?php } else { ?>
                            <?= ambil_nama_by_id("tamu", "jabatan", "tamu_id", $t->dihadiri); ?>
                        <?php } ?>
                    </td>
                    <td>
                        <?= ($t->status == "1") ? 'Hadir' : 'Tidak Hadir'; ?>
                    </td>
                    <td>
                        <?php
                        if (!empty($cek_check_in)) {
                            if ($tanggal == "all") {
                                $no_check_in = 1;
                                foreach ($cek_check_in as $ci) :
                                    if ($no_check_in > 1) echo "<br>";
                        ?>
                                    <?= tgl_full($ci->tanggal_check_in . " " . $ci->jam_check_in); ?>
                        <?php
                                    $no_check_in++;
                                endforeach;
                            } else {
                                echo tgl_full($cek_check_in->tanggal_check_in . " " . $cek_check_in->jam_check_in);
                            }
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="footer">
        <p>Daftar Hadir ini dicetak dari layanan ngundang.in dan dapat digunakan untuk pertanggungjawaban kegiatan</p>
    </div>
</body>

</html>