<?php $this->load->view("frontend/inc/head_html"); ?>
<section class="section" id="services">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8">
                <div class="title text-center mb-5">
                    <h3 class="font-weight-normal text-dark">Cek <span class="text-warning">Undangan</span></h3>
                    <p class="text-muted">Undangan ini terdaftar di ngundang.in dengan rincian seperti dibawah ini</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="service-box service-warning">
                    <div class="mb-5">
                        <i class="pe-7s-user service-icon"></i>
                    </div>
                    <h5 class="service-title text-dark font-weight-normal pt-1 mb-4">Panitia</h5>
                    <p class="text-muted service-subtitle mb-4">ICT Watch, Whatsapp dan Kementerian Komunikasi dan Informatika</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="service-box service-warning">
                    <div class="mb-5">
                        <i class="pe-7s-star service-icon"></i>
                    </div>
                    <h5 class="service-title text-dark font-weight-normal pt-1 mb-4">Kegiatan</h5>
                    <p class="text-muted service-subtitle mb-4"><?= $undangan->judul; ?></p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="service-box service-warning">
                    <div class="mb-5">
                        <i class="pe-7s-timer service-icon"></i>
                    </div>
                    <h5 class="service-title text-dark font-weight-normal pt-1 mb-4">Waktu</h5>
                    <p class="text-muted service-subtitle mb-4"><?= tgl_full($undangan->tanggal . " " . $undangan->jam); ?></p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="service-box service-warning">
                    <div class="mb-5">
                        <i class="pe-7s-map-marker service-icon"></i>
                    </div>
                    <h5 class="service-title text-dark font-weight-normal pt-1 mb-4">Lokasi</h5>
                    <p class="text-muted service-subtitle mb-4"><?= $undangan->nama_lokasi; ?></p>
                </div>
            </div>
        </div>

        <!-- <p>Daftar Hadir ini dicetak dari layanan <a href="https://toduwo.id">toduwo.id</a> dan dapat digunakan untuk pertanggungjawaban kegiatan.</p> -->
    </div>
</section>
<?php $this->load->view("frontend/inc/foot_html"); ?>