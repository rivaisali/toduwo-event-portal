<div class="row">
    <div class="col-sm-12">
        <div class="user-info-list card">
            <div class="card-header card-header-divider">
                Data Undangan
                <div class="tools dropdown">
                    <a href="<?= base_url($base); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-arrow-back text-white"></span> Kembali
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <table class="no-border no-strip skills">
                            <tbody class="no-border-x no-border-y">
                                <tr>
                                    <td class="icon"><span class="mdi mdi-badge-check"></span></td>
                                    <td class="item">Judul<span class="icon s7-portfolio"></span></td>
                                    <td><?= $data->judul ?></td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-storage"></span></td>
                                    <td class="item">Keterangan<span class="icon s7-gift"></span></td>
                                    <td><?= $data->keterangan; ?></td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-calendar-check"></span></td>
                                    <td class="item">Waktu<span class="icon s7-phone"></span></td>
                                    <td>
                                        <?php if ($data->tanggal == $data->tanggal_selesai) {
                                            echo tgl_full($data->tanggal . " " . $data->jam) . " Sampai " . jam($data->tanggal_selesai . " " . $data->jam_selesai);
                                        } else {
                                            echo tgl_full($data->tanggal . " " . $data->jam) . " Sampai " . tgl_full($data->tanggal_selesai . " " . $data->jam_selesai);
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-home"></span></td>
                                    <td class="item">Lokasi<span class="icon s7-map-marker"></span></td>
                                    <td><?= $data->nama_lokasi; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-pin"></span></td>
                                    <td class="item">Alamat<span class="icon s7-global"></span></td>
                                    <td><?= $data->alamat_lokasi; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="no-border no-strip skills">
                            <tbody class="no-border-x no-border-y">
                                <tr>
                                    <td class="icon"><span class="mdi mdi-accounts"></span></td>
                                    <td class="item">Jumlah Tamu<span class="icon s7-portfolio"></span></td>
                                    <td><?= count($tamu); ?></td>
                                </tr>
                                <?php if ($data->jenis == "online") : ?>
                                    <tr>
                                        <td class="icon"><span class="mdi mdi-link"></span></td>
                                        <td class="item">Link Absen<span class="icon s7-portfolio"></span></td>
                                        <td>
                                            <?= base_url("absen/" . $data->undangan_id); ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-palette"></span></td>
                                    <td class="item">Format Sertifikat<span class="icon s7-portfolio"></span></td>
                                    <td>
                                        <?= ambil_nama_by_id('format_sertifikat', 'judul', 'format_sertifikat_id', $data->format_sertifikat_id); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-help"></span></td>
                                    <td class="item">Pertanyaan Absen<span class="icon s7-portfolio"></span></td>
                                    <td>
                                        <?= $data->pertanyaan_absen; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <?php if ($data->absen == "buka") : ?>
                                        <td colspan="3"><a href="<?= base_url("tutup-absen/" . $data->undangan_id); ?>" class="btn btn-danger btn-sm">Tutup Absen</a></td>
                                    <?php else : ?>
                                        <td colspan="3"><a href="<?= base_url("buka-absen/" . $data->undangan_id); ?>" class="btn btn-primary btn-sm">Buka Absen</a></td>
                                    <?php endif; ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">
                Daftar Tamu
                <div class="tools dropdown">
                    <a target="_blank" href="<?= base_url("undangan/export-peserta/" . $data->undangan_id); ?>" class="btn btn-space btn-success">
                        <span class="icon icon-left mdi mdi-download text-white"></span> Export Excel
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Kode Undangan</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($tamu as $t) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $t->tamu_undangan_id; ?></td>
                                <td><?= $t->nama_lengkap; ?></td>
                                <td><?= $t->alamat; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>