<?php
if ($status == "1") {
    $alert  =   "success";
} else {
    $alert  =   "danger";
}
?>
<div class="alert alert-<?= $alert; ?> alert-dismissible" role="alert">
    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
    <div class="message">
        <?= $msg; ?>
    </div>
</div>
<?php if (isset($pertanyaan)) { ?>
    <?= form_open("jawab-absen"); ?>
    <input type="hidden" name="undangan_id" value="<?= $id; ?>">
    <input type="hidden" name="kode" value="<?= $kode; ?>">
    <div class="form-row">
        <?php foreach ($pertanyaan as $per) : ?>
            <div class="form-group col-md-12 mb-4">
                <label><?= $per->pertanyaan; ?></label>
                <input type="hidden" name="pertanyaan[]" value="<?= $per->pertanyaan; ?>">
                <select name="jawaban[]" class="form-control" required>
                    <option value="">- Pilih -</option>
                    <option value="<?= $per->opsi_a; ?>"><?= $per->opsi_a; ?></option>
                    <option value="<?= $per->opsi_b; ?>"><?= $per->opsi_b; ?></option>
                    <option value="<?= $per->opsi_c; ?>"><?= $per->opsi_c; ?></option>
                    <option value="<?= $per->opsi_d; ?>"><?= $per->opsi_d; ?></option>
                </select>
            </div>
        <?php endforeach; ?>
        <div class="form-group col-md-12 mb-4">
            <label>Apakah Anda berminat belajar lebih lanjut tentang Literasi Digital secara gratis dan berkualitas?</label>
            <input type="hidden" name="pertanyaan[]" value="Apakah Anda berminat belajar lebih lanjut tentang Literasi Digital secara gratis dan berkualitas?">
            <select name="jawaban[]" class="form-control" required>
                <option value="">- Pilih -</option>
                <option value="Berminat">Berminat</option>
                <option value="Belum Berminat">Belum Berminat</option>
            </select>
        </div>
        <div class="form-group col-md-12 mb-4">
            <label>Tuliskan kesan Anda untuk acara ini , khususnya terkait materi yang paling menarik:</label>
            <input type="hidden" name="pertanyaan[]" value="Tuliskan kesan Anda untuk acara ini , khususnya terkait materi yang paling menarik:">
            <textarea name="jawaban[]" rows="2" class="form-control" required></textarea>
        </div>
        <div class="form-group col-md-12 mb-4">
            <label>Tuliskan saran Anda untuk pengembangan program edukasi Literasi Digital:</label>
            <input type="hidden" name="pertanyaan[]" value="Tuliskan saran Anda untuk pengembangan program edukasi Literasi Digital:">
            <textarea name="jawaban[]" rows="2" class="form-control" required></textarea>
        </div>
    </div>
    <button class="btn btn-primary btn-block" type="submit">Jawab</button>
<?php } ?>
<?= form_close(); ?>