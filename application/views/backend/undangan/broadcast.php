<?= form_open($base . "/kirim-pesan-broadcast", [
    "autocomplete" => "off",
    "id" => "formBroadcast"
]) ?>
<?= form_hidden('undangan_id', $data->undangan_id); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="user-info-list card">
            <div class="card-header card-header-divider">
                Data Pesan
                <div class="tools dropdown">
                    <a href="<?= base_url($base . "/riwayat-broadcast/" . $data->undangan_id); ?>" class="btn btn-space btn-secondary">
                        <span class="icon icon-left mdi mdi-time-restore text-dark"></span> Riwayat Broadcast
                    </a>
                    <a href="<?= base_url($base . "/detail/" . $data->undangan_id); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-arrow-back text-white"></span> Kembali
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 order-sm-2 order-lg-1">
                        <div class="form-group pt-1">
                            <label for="judul">Judul</label>
                            <input class="form-control form-control-sm <?= form_error('judul') ? 'is-invalid' : ''; ?>" name="judul" id="judul" type="text" placeholder="Judul" value="<?= set_value('judul', ''); ?>" required="required" autofocus>
                            <span class="text-muted mt-1 d-block">Judul tidak akan dimasukkan ke dalam pesan WA.</span>
                            <?= form_error('judul'); ?>
                        </div>
                        <div class="form-group pt-1">
                            <label for="isi_pesan">Isi Pesan</label>
                            <textarea name="isi_pesan" id="isi_pesan" class="form-control form-control-sm <?= form_error('isi_pesan') ? 'is-invalid' : ''; ?>" rows="8" placeholder="Masukkan pesan disini. Dengan mengukuti aturan penulisan" required="required"><?= set_value('isi_pesan', '') ?></textarea>
                            <?= form_error('isi_pesan'); ?>
                        </div>
                    </div>

                    <div class="col-lg-6 order-sm-1 order-lg-2">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-title">Aturan Penulisan Pesan</div>
                                <div class="aturan-pesan">
                                    <code>*$pesan*</code> : <b>Tulisan Tebal</b><br>
                                </div>
                                <div class="aturan-pesan">
                                    <code>_$pesan_</code> : <i>Tulisan Miring</i><br>
                                </div>
                                <div class="aturan-pesan">
                                    <code>~$pesan~</code> : <s>Tulisan Bergaris</s><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">
                Daftar Tamu
                <div class="tools dropdown">
                    <button type="submit" class="btn btn-space btn-success kirimBC">
                        <span class="icon icon-left mdi mdi-whatsapp text-white"></span> Kirim
                    </button>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="broadcast">
                    <thead>
                        <tr>
                            <th>
                                <div class="be-checkbox custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="checkAll">
                                    <label class="custom-control-label ml-6" for="checkAll">Pilih Semua</label>
                                </div>
                            </th>
                            <th>Kode Undangan</th>
                            <th>Nama</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($tamu as $t) : ?>
                            <tr>
                                <td>
                                    <div class="be-checkbox custom-control custom-checkbox">
                                        <input name="tamu[<?= $t->tamu_id; ?>]" value="<?= $t->no_telp; ?>" class="custom-control-input pilihTamuBroadcast" type="checkbox" id="check<?= $no; ?>" <?= set_checkbox("tamu[$t->tamu_id]", $t->no_telp); ?> <?= ($t->whatsapp == "0") ? 'disabled' : ''; ?>>
                                        <label class="custom-control-label ml-6" for="check<?= $no; ?>">Pilih</label>
                                    </div>
                                </td>
                                <td><?= $t->tamu_undangan_id; ?></td>
                                <td>
                                    <?= $t->nama_lengkap; ?><br>
                                    (<?= $t->no_telp; ?>)
                                </td>
                            </tr>
                        <?php $no++;
                        endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?= form_close() ?>