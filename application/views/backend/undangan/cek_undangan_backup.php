<?php $this->load->view("frontend/inc/head_html"); ?>

<!-- About us Start -->
<section class="section bg-light my-0 py-4" id="about">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8">
                <div class="title text-center mb-5">
                    <h3 class="font-weight-normal text-dark">toduwo<span class="text-warning">.id</span></h3>
                    <!-- <p class="text-muted">Toduwo Event merupakan layanan yang dibuat untuk memudahkan Komunitas, grup dan lainnya dalam kegiatan Undangan digital yang terintegrasi dengan buku tamu digital sehingga lebih memudahkan.</p> -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <h2 class="line-height-1_6 text-dark mb-4"><?= $undangan->judul; ?></h2>
            </div>
            <div class="col-md-7 offset-md-1">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="text-dark font-weight-light f-20 mb-3"><i class="mdi mdi-map-marker-radius"></i> Lokasi</h6>
                        <p class="text-muted font-weight-light"><?= $undangan->nama_lokasi; ?></p>
                    </div>
                    <div class="col-md-6">
                        <h6 class="text-dark font-weight-light f-20 mb-3"><i class="mdi mdi-calendar-month-outline"></i> Waktu</h6>
                        <p class="text-muted font-weight-light"><?= tgl_full($undangan->tanggal . " " . $undangan->jam); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About us End -->

<section class="section my-0 py-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8">
                <div class="title text-center mb-3">
                    <h3 class="font-weight-normal text-primary">Mendaftar</h3>
                </div>
            </div>
        </div>

        <?php if ($this->session->flashdata("notifikasi")) { ?>
            <div class="container mt-2">
                <div class="alert alert-<?= $this->session->flashdata("notifikasi")["status"] ?> alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="message">
                        <?= $this->session->flashdata("notifikasi")["msg"]; ?>
                    </div>
                </div>
            </div>
        <?php } else { ?>

            <?= form_open(""); ?>
            <?= form_hidden('undangan_id', $undangan->undangan_id); ?>
            <div class="row p-2 rounded-lg">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" name="nama" id="nama" class="form-control <?= form_error('nama') ? 'is-invalid' : 'border-primary'; ?>" value="<?= set_value('nama', '') ?>">
                        <?= form_error('nama'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="no_telp">Telp / WA</label>
                        <input type="text" name="no_telp" id="no_telp" class="form-control <?= form_error('no_telp') ? 'is-invalid' : 'border-primary'; ?>" value="<?= set_value('no_telp', '') ?>">
                        <?= form_error('no_telp'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" name="alamat" id="alamat" class="form-control <?= form_error('alamat') ? 'is-invalid' : 'border-primary'; ?>" value="<?= set_value('alamat', '') ?>">
                        <?= form_error('alamat'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control <?= form_error('email') ? 'is-invalid' : 'border-primary'; ?>" value="<?= set_value('email', '') ?>">
                        <?= form_error('email'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="instansi">Instansi</label>
                        <input type="text" name="instansi" id="instansi" class="form-control <?= form_error('instansi') ? 'is-invalid' : 'border-primary'; ?>" value="<?= set_value('instansi', '') ?>">
                        <?= form_error('instansi'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="jabatan">Jabatan</label>
                        <input type="text" name="jabatan" id="jabatan" class="form-control <?= form_error('jabatan') ? 'is-invalid' : 'border-primary'; ?>" value="<?= set_value('jabatan', '') ?>">
                        <?= form_error('jabatan'); ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-block my-2">Daftar</button>
                </div>
            </div>
            <?= form_close(); ?>

        <?php } ?>
    </div>
</section>

<?php $this->load->view("frontend/inc/foot_html"); ?>