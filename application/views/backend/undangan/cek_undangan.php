<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= base_url(); ?>assets_admin/img/logo-icon.png">
    <title>Ngundang.in - Solusi Undangan Digital untuk Event</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/my.css">
    <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
    <script>
        const url = '<?php echo base_url(); ?>';
    </script>
</head>

<body>

    <div class="header-bar">
        <div class="logo">
            <img src="<?= base_url(); ?>assets/images/event-logo.png" alt="" height="60">
        </div>
        <div class="title">Formulir Pendaftaran Event</div>


    </div>


    <!-- About us Start -->
    <section class="section">
        <div class="container pt-4 px-lg-5">
            <div id="about">
                <div class="row">
                    <div class="col-12 text-center">
                        <img src="<?= base_url("assets/images/logo-3.png"); ?>" width="250" alt="logo">
                        <h2 class="mb-1"><?= $undangan->judul; ?></h2>
                        <?php if ($undangan->tanggal == $undangan->tanggal_selesai) { ?>
                            <span class="event-date"><?= hari($undangan->tanggal) . ", " . tgl_indonesia($undangan->tanggal); ?></span>
                            <span class="event-date">Pukul: <?= jam($undangan->tanggal . " " . $undangan->jam) . " WIB - " . jam($undangan->tanggal_selesai . " " . $undangan->jam_selesai) . " WIB"; ?></span>
                        <?php } else { ?>
                            <span class="event-date"><?= hari($undangan->tanggal) . ", " . tgl_indonesia($undangan->tanggal); ?> - <?= hari($undangan->tanggal_selesai) . ", " . tgl_indonesia($undangan->tanggal_selesai); ?></span>
                            <span class="event-date">Pukul: <?= jam($undangan->tanggal . " " . $undangan->jam) . " WIB - " . jam($undangan->tanggal_selesai . " " . $undangan->jam_selesai) . " WIB"; ?></span>
                        <?php } ?>
                        <span class="event-date"><?= $undangan->nama_lokasi; ?></span>
                        <?= ($undangan->alamat_lokasi != "") ? "<span class='event-date'>(" . $undangan->alamat_lokasi . ")</span>" : ""; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About us End -->

    <section class="section m-0 p-2">
        <div class="container">
            <div class="px-lg-5">
                <div class="card mb-3 shadow-lg">
                    <div class="card-body">

                        <?php if ($undangan->pendaftaran == "buka") { ?>
                            <?php if ($this->session->flashdata("notifikasi")) { ?>
                                <div class="alert alert-<?= $this->session->flashdata("notifikasi")["status"] ?> alert-dismissible" role="alert">
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                                    <div class="message">
                                        <?= $this->session->flashdata("notifikasi")["msg"]; ?>
                                    </div>
                                </div>
                            <?php } else { ?>

                                <?= form_open("", ["id" => "registrasi"]); ?>
                                <?= form_hidden('undangan_id', $undangan->undangan_id); ?>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="nama">Nama Lengkap</label>
                                        <input type="text" name="nama" id="nama" class="form-control <?= form_error('nama') ? 'is-invalid' : ''; ?>" value="<?= set_value('nama', '') ?>" required>
                                        <?= form_error('nama'); ?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="nama_panggilan">Nama Panggilan</label>
                                        <input type="text" name="nama_panggilan" id="nama_panggilan" class="form-control <?= form_error('nama_panggilan') ? 'is-invalid' : ''; ?>" value="<?= set_value('nama_panggilan', '') ?>" required>
                                        <?= form_error('nama_panggilan'); ?>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="email">Alamat E-Mail</label>
                                        <input type="email" name="email" id="email" class="form-control <?= form_error('email') ? 'is-invalid' : ''; ?>" value="<?= set_value('email', '') ?>" required>
                                        <?= form_error('email'); ?>
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="no_telp">No Telepon / WA</label>
                                        <input type="text" name="no_telp" id="no_telp" class="form-control <?= form_error('no_telp') ? 'is-invalid' : ''; ?>" value="<?= set_value('no_telp', '') ?>" required>
                                        <?= form_error('no_telp'); ?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="jenis_kelamin">Jenis Kelamin</label>
                                        <select class="form-control" name="jenis_kelamin" id="jenis_kelamin" class="form-control <?= form_error('jenis_kelamin') ? 'is-invalid' : ''; ?>" required>
                                            <option value="">- Pilih - </option>
                                            <option value="Laki-laki" <?= set_select('jenis_kelamin', 'Laki-laki') ?>>Laki-Laki</option>
                                            <option value="Perempuan" <?= set_select('jenis_kelamin', 'Perempuan') ?>>Perempuan</option>
                                        </select>
                                        <?= form_error('jenis_kelamin'); ?>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="usia">Usia</label>
                                        <input type="text" name="usia" id="usia" class="form-control <?= form_error('usia') ? 'is-invalid' : ''; ?>" value="<?= set_value('usia', '') ?>">
                                        <?= form_error('usia'); ?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="pekerjaan">Pekerjaan</label>
                                        <select class="form-control" name="pekerjaan" id="pekerjaan" class="form-control <?= form_error('pekerjaan') ? 'is-invalid' : ''; ?>" required>
                                            <option value="">- Pilih -</option>
                                            <option value="Guru/Dosen/Pengajar" <?= set_select('pekerjaan', 'Guru/Dosen/Pengajar') ?>>Guru/Dosen/Pengajar</option>
                                            <option value="Pelajar / Mahasiswa" <?= set_select('pekerjaan', 'Pelajar / Mahasiswa') ?>>Pelajar / Mahasiswa</option>
                                            <option value="Pegawai (Negeri/Swasta) / Profesional" <?= set_select('pekerjaan', 'Pegawai (Negeri/Swasta) / Profesional') ?>>Pegawai (Negeri/Swasta) / Profesional</option>
                                            <option value="Komunitas" <?= set_select('pekerjaan', 'Komunitas') ?>>Komunitas</option>
                                            <option value="Wiraswasta" <?= set_select('pekerjaan', 'Wiraswasta') ?>>Wiraswasta</option>
                                            <option value="Lainnya" <?= set_select('pekerjaan', 'Lainnya') ?>>Lainnya</option>
                                        </select>
                                        <?= form_error('pekerjaan'); ?>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="pendidikan">Pendidikan</label>
                                            <select name="pendidikan" name="pendidikan" id="pendidikan" class="form-control <?= form_error('pendidikan') ? 'is-invalid' : ''; ?>" required>
                                                <option value="">- Pilih -</option>
                                                <option value="SD / Sederajat" <?= set_select('pendidikan', 'SD / Sederajat') ?>>SD / Sederajat</option>
                                                <option value="SMP / Sederajat" <?= set_select('pendidikan', 'SMP / Sederajat') ?>>SMP / Sederajat</option>
                                                <option value="SMA / Sederajat" <?= set_select('pendidikan', 'SMA / Sederajat') ?>>SMA / Sederajat</option>
                                                <option value="D1" <?= set_select('pendidikan', 'D1') ?>>D1</option>
                                                <option value="D2" <?= set_select('pendidikan', 'D2') ?>>D2</option>
                                                <option value="D3" <?= set_select('pendidikan', 'D3') ?>>D3</option>
                                                <option value="D4 / S1" <?= set_select('pendidikan', 'D4 / S1') ?>>D4 / S1</option>
                                                <option value="S2" <?= set_select('pendidikan', 'S2') ?>>S2</option>
                                                <option value="S3" <?= set_select('pendidikan', 'S3') ?>>S3</option>
                                            </select>
                                            <?= form_error('pendidikan'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="instansi">Instansi / Komunitas</label>
                                            <input type="text" name="instansi" id="instansi" class="form-control <?= form_error('instansi') ? 'is-invalid' : ''; ?>" value="<?= set_value('instansi', '') ?>" required>
                                            <?= form_error('instansi'); ?>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h4>Domisili</h4>
                                <?php if ($undangan->jenis_form == "2") { ?>
                                    <div class="alert alert-info">
                                        Masukkan Alamat lengkap anda. Dimungkinkan sampai memasukkan detail tempat yang mudah di temukan. Contoh: (Samping Mesjid Baiturrahman).<br>
                                        Alamat anda dibutuhkan untuk pengantaran makan siang bagi 300 pendaftar pertama.
                                    </div>

                                    <hr>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="alamat">Alamat</label>
                                            <textarea name="alamat" id="alamat" rows="2" class="form-control <?= form_error('alamat') ? 'is-invalid' : ''; ?>" required><?= set_value('alamat'); ?></textarea>
                                            <?= form_error('alamat'); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="row row-cols-2">
                                    <div class="col mb-2">
                                        <label for="propinsi">Propinsi</label>
                                        <select name="propinsi" name="propinsi" id="propinsi" class="form-control <?= form_error('propinsi') ? 'is-invalid' : ''; ?> select2 col-md-11" required>
                                            <option value="">- Pilih -</option>
                                            <?php foreach ($propinsi as $prop) : ?>
                                                <option value="<?= $prop->namapropinsi; ?>"><?= $prop->namapropinsi; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <?= form_error('propinsi'); ?>
                                    </div>

                                    <div class="col mb-2">
                                        <label for="kota">Kab / Kota</label>
                                        <select name="kota" name="kota" id="kota" class="form-control <?= form_error('kota') ? 'is-invalid' : ''; ?> select2 col-md-11" required>
                                            <option value="">- Pilih -</option>
                                        </select>
                                        <?= form_error('kota'); ?>
                                    </div>
                                    <?php if ($undangan->jenis_form == "2") { ?>
                                        <div class="col mb-2">
                                            <label for="kecamatan">Kecamatan</label>
                                            <select name="kecamatan" name="kecamatan" id="kecamatan" class="form-control <?= form_error('kecamatan') ? 'is-invalid' : ''; ?> select2 col-md-11" required>
                                                <option value="">- Pilih -</option>
                                            </select>
                                            <?= form_error('kecamatan'); ?>
                                        </div>

                                        <div class="col mb-2">
                                            <label for="kelurahan">Kelurahan</label>
                                            <select name="kelurahan" name="kelurahan" id="kelurahan" class="form-control <?= form_error('kelurahan') ? 'is-invalid' : ''; ?> select2 col-md-11" required>
                                                <option value="">- Pilih -</option>
                                            </select>
                                            <?= form_error('kelurahan'); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="form-row mt-2">
                                    <div class="form-group col-md-12">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1" required>
                                            <label class="custom-control-label" for="customCheck1">Dengan mendaftar kegiatan ini berarti saya berkomitmen untuk mengikuti seluruh rangkaian kegiatan <b><?= $undangan->judul; ?></b></label>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <button class="btn btn-primary btn-block" type="submit" id="btn-daftar-event">Daftar Event</button>
                                <?= form_close() ?>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="alert alert-info text-center">
                                Pendaftaran <?= $undangan->judul; ?> Telah di <b>TUTUP</b>.<br>
                                Terima Kasih atas Atensi dan Semangatnya dalam mengikuti Kegiatan ini.
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="<?= base_url("assets/js/my.js"); ?>"></script>
</body>

</html>
<?php //$this->load->view("frontend/inc/foot_html"); 
?>