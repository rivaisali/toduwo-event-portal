<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?>
                <span class="text-muted small"><?= count($data) . "/" . user("penerima_tamu"); ?></span>
                <div class="tools dropdown">
                    <?php if (user("penerima_tamu") != "0" && count($data) < user("penerima_tamu")) { ?>
                        <a href="<?= base_url($base . "/tambah"); ?>" class="btn btn-space btn-primary">
                            <span class="icon icon-left mdi mdi-plus text-white"></span> Tambah Data
                        </a>
                    <?php } ?> ?>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>Nama Penerima Tamu</th>
                            <th>Kode Password</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $d) : ?>
                            <tr class="odd gradeX">
                                <td><?= $d->nama; ?></td>
                                <td>
                                    <?= $d->passkey; ?>
                                </td>
                                <td class="actions">
                                    <a class="icon mx-1" href="<?= base_url($base . "/ubah/" . $d->penerima_tamu_id); ?>">
                                        <i class="mdi mdi-edit"></i>
                                    </a>
                                    <a class="icon mx-1" href="<?= base_url($base . "/hapus/" . $d->penerima_tamu_id); ?>" id="hapusData">
                                        <i class="mdi mdi-delete text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>