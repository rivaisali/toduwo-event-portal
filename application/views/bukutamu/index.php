<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

	<title>Buku Tamu - <?=$data->judul;?></title>
	<!-- Bootstrap core CSS -->
	<link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Berkshire+Swash&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="<?=base_url();?>assets/css/jquery-ui.css" />
	<!-- Custom styles for this template -->
	<link href="<?=base_url();?>assets/css/style.css" rel="stylesheet">
</head>

<body>

	<!-- <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">Deno & Rivai</h5>

      <a class="btn btn-outline-primary" href="#">Masuk</a>
    </div> -->
	<div class="img-head">
	</div>

	<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
		<h1 class="display-4"><?=$data->judul;?></h1>
		<p class="lead"><strong>Buku Tamu</strong></p>
	</div>

	<div class="container">

		<ul class="pagination" id="list-camera">



		</ul>


		<div class="card-deck mb-3 text-center">
			<div class="card mb-4 box-shadow">

				<div class="card-header">
					<h4 class="my-0 font-weight-normal">Scan Qrcode</h4>
				</div>
				<div class="card-body">
					<video width="100%" height="100%" id="preview"></video>
					<span class="badge badge-warning">Arahkan QRcode Undangan ke Kamera</span>
				</div>
			</div>
			<div class="card mb-4 box-shadow">
				<div class="card-header">
					<h4 class="my-0 font-weight-normal">Pencarian Nama</h4>
				</div>
				<div class="card-body">

					<div class="form-group">
						<label for="usr">Nama Lengkap</label>
						<input type="text" class="form-control" placeholder="Masukan Nama Anda" required id="pencarian">
						<input type="hidden" class="form-control" id="hasil">
						<input type="hidden" class="form-control" id="code">
					</div>
					<button class="btn btn-lg btn-block btn-primary" id="checkin">Check IN</button>

					<figure class="mt-4">
						<img src="<?=base_url("uploads/qrcode/regis-manual-" . $data->undangan_id . ".png");?>" class="img-thumbnail" width="30%">
						<figcaption class="text-uppercase"><span class="h5 text-primary d-block my-0">scan barcode</span> Khusus yang tidak mendapatkan undangan digital</figcaption>
					</figure>
				</div>
			</div>

		</div>


	</div>

	<div class="footer">

	</div>

	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!--<script src="js/bootstrap.min.js"></script>-->
	<script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script src="<?=base_url();?>assets/js/jquery-1.12.4.js"></script>
	<script src="<?=base_url();?>assets/js/jquery-ui.js"></script>

	<script>
		$(document).ready(function() {
			$("#pencarian").focus();


			$("#checkin").attr("disabled", true);
			$("#pencarian").autocomplete({
				source: "<?=base_url();?>bukutamu/tamu/<?=$data->undangan_id;?>",
				minLength: 2,
				select: function(event, ui) {
					$("#hasil").val(ui.item.value);
					$("#code").val(ui.item.code);
					var hasil = $("#hasil").val();
					if (hasil == "") {
						$("#checkin").attr("disabled", true);
					} else if (hasil == "Data tidak ditemukan") {
						$("#checkin").attr("disabled", true);
					} else {
						$("#checkin").attr("disabled", false);
					}

				}

			});

		});


		$("#checkin").click(function() {
			var code = $("#code").val();
			if (code == "" || code == null) {
				alert("Nama Harus dipilih");
			} else {
				$.ajax({
					url: "<?=base_url();?>bukutamu/checkin/<?=$data->undangan_id;?>",
					type: 'POST',
					data: {
						'code': code
					},
					success: function(result) {
						console.log(result);
						var res = JSON.parse(result);
						if (res['hasil'] == "sukses") {
							popup_success(res['nama_lengkap']);

							$("#pencarian").val("");
							$("#code").val("");
							$("#hasil").val("");
							$("#checkin").attr("disabled", false);
							$("#pencarian").focus();
						} else {
							popup_error(res['error']);

							$("#pencarian").val("");
							$("#code").val("");
							$("#hasil").val("");
							$("#checkin").attr("disabled", false);
							$("#pencarian").focus();
						}

					}
				});
			}
		});

		function popup_success(nama) {
			Swal.fire({
				position: 'center',
				icon: 'success',
				title: 'Selamat datang,<br>' + nama,
				showConfirmButton: false,
				timer: 4000,
				width: 600
			}).then(
				function() {
					$("#checkin").attr("disabled", true);
					$("#pencarian").focus();
				}

			)

		}

		function popup_error(pesan) {
			Swal.fire({
				position: 'center',
				icon: 'error',
				title: pesan,
				showConfirmButton: false,
				timer: 4000
			}).then(
				function() {
					$("#checkin").attr("disabled", true);
					$("#pencarian").focus();
				}

			)

		}

		let scanner = new Instascan.Scanner({
			video: document.getElementById('preview')
		});
		scanner.addListener('scan', function(content) {
			$.ajax({
				url: "<?=base_url();?>bukutamu/checkin/<?=$data->undangan_id;?>",
				type: 'POST',
				data: {
					'code': content
				},
				success: function(result) {
					var res = JSON.parse(result);
					if (res['hasil'] == "sukses") {
						popup_success(res['nama_lengkap']);
					} else {
						popup_error(res['error']);
					}

				}
			});
		});

		Instascan.Camera.getCameras().then(function(cameras) {
			var i = 0;
			var html;
			if (cameras.length > 0) {
				html = '<li class="page-item disabled">' +
					'<a class="page-link" href="#" tabindex="-1">Daftar Camera</a></li>';
				for (var i = 0; i < cameras.length; i++) {
					html += '<li class="page-item"><a class="page-link cam" data-id="' + i + '">' + (i + 1) + '</a></li>';

				}
				$('#list-camera').html(html);
				$(document).on('click', '.cam', function() {
					var id = $(this).data("id");
					select_camera(id);
				});

				scanner.start(cameras[0]);

			} else {
				console.error('No cameras found.');
			}
		}).catch(function(e) {
			console.error(e);
		});



		function select_camera(id) {
			Instascan.Camera.getCameras().then(function(cameras) {
				scanner.start(cameras[id]);
			}).catch(function(e) {
				console.error(e);
			});
		}
	</script>
</body>

</html>