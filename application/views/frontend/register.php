<?php $this->load->view('frontend/inc/head_html'); ?>

<div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="double-bounce1"></div>
			<div class="double-bounce2"></div>
		</div>
	</div>
</div>

<div class="back-to-home rounded d-none d-sm-block">
	<a href="<?= base_url(); ?>" class="btn btn-icon btn-soft-primary"><i data-feather="home" class="icons"></i></a>
</div>

<!-- Hero Start -->
<section class="bg-home">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-6 d-none d-md-block">
				<div class="mr-lg-5">
					<img src="<?= base_url(); ?>assets/images/user/signup.svg" class="img-fluid d-block mx-auto" alt="">
				</div>
			</div>
			<div class="col-lg-5 col-md-6">
				<div class="card login_page shadow rounded border-0">
					<div class="card-body py-1">
						<h4 class="card-title text-center">Mendaftar Di Ngundang.in</h4>
						<!-- <form action="" method="POST" class="login-form mt-4"> -->

						<?php if ($this->session->flashdata("notifikasi")) { ?>
							<div class="alert alert-<?= $this->session->flashdata("notifikasi")["status"] ?>">
								<?= $this->session->flashdata("notifikasi")["msg"]; ?>
							</div>
						<?php } ?>

						<!-- <?php echo validation_errors(); ?> -->
						<?= form_open("", ["class" => "login-form mt-4", "method" => "POST"]); ?>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group position-relative mb-1">
									<label>Paket <span class="text-danger">*</span></label>
									<i data-feather="list" class="fea icon-sm icons"></i>
									<select class="form-control custom-select <?= (form_error('paket')) ? 'is-invalid' : ''; ?> pl-5" name="paket" id="paket" autofocus>
										<option selected="" value="0" disabled>Pilih Paket</option>
										<?php foreach ($paket as $p) : ?>
											<option value="<?= $p->paket_id; ?>" <?= set_select('paket', $p->paket_id); ?>>
												<?= $p->nama_paket; ?> (<?= rupiah($p->harga); ?>)
											</option>
										<?php endforeach; ?>
									</select>
									<?php echo form_error('paket'); ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group position-relative mb-1">
									<label>Daftar Sebagai <span class="text-danger">*</span></label>
									<i data-feather="users" class="fea icon-sm icons"></i>
									<select class="form-control custom-select <?= (form_error('tipe')) ? 'is-invalid' : ''; ?> pl-5" name="tipe">
										<option selected="" value="0" disabled>Pilih Tipe</option>
										<option value="1" <?= set_select('tipe', '1'); ?>>
											Calon Pengantin Pria
										</option>
										<option value="2" <?= set_select('tipe', '2'); ?>>
											Calon Pengantin Wanita
										</option>
										<option value="3" <?= set_select('tipe', '3'); ?>>
											Keluarga
										</option>
										<option value="4" <?= set_select('tipe', '4'); ?>>
											Panitia
										</option>
									</select>
									<?php echo form_error('tipe'); ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group position-relative mb-1">
									<label>Nama Lengkap <span class="text-danger">*</span></label>
									<i data-feather="user" class="fea icon-sm icons"></i>
									<input type="text" class="form-control <?= (form_error('nama')) ? 'is-invalid' : ''; ?> pl-5" placeholder="Nama Lengkap" name="nama" value="<?= set_value('nama', ''); ?>" required>
									<?php echo form_error('nama'); ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group position-relative mb-1">
									<label>Telp / WA <span class="text-danger">*</span></label>
									<i data-feather="phone" class="fea icon-sm icons"></i>
									<input type="text" class="form-control pl-5 <?= (form_error('telp')) ? 'is-invalid' : ''; ?>" placeholder="Telp / WA" name="telp" value="<?= set_value('telp', ''); ?>" required>
									<?php echo form_error('telp'); ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group position-relative mb-1">
									<label>Email <span class="text-danger">*</span></label>
									<i data-feather="mail" class="fea icon-sm icons"></i>
									<input type="email" class="form-control pl-5 <?= (form_error('email')) ? 'is-invalid' : ''; ?>" placeholder="Email" name="email" value="<?= set_value('email', ''); ?>" required>
									<?php echo form_error('email'); ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group position-relative mb-1">
									<label>Password <span class="text-danger">*</span></label>
									<i data-feather="key" class="fea icon-sm icons"></i>
									<input type="password" class="form-control pl-5 <?= (form_error('password')) ? 'is-invalid' : ''; ?>" placeholder="Password" name="password" required>
									<?php echo form_error('password'); ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group position-relative mb-1">
									<label>Kode Promo</label>
									<i data-feather="link" class="fea icon-sm icons"></i>
									<input type="text" id="kodePromo" class="form-control pl-5" name="kode_promo" placeholder="Kode Promo">
									<span id="msgPromo" class="my-2 badge"></span>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="customCheck1" required="">
										<label class="custom-control-label" for="customCheck1">Saya Setuju <a href="#" class="text-primary">Terms And Condition</a></label>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<input type="submit" name="daftar" value="Daftar" class="btn btn-primary btn-block">
							</div>

							<div class="mx-auto">
								<p class="mb-0 mt-3"><small class="text-dark mr-2">Already have an account ?</small> <a href="<?= base_url("login"); ?>" class="text-dark font-weight-bold">Sign in</a></p>
							</div>
						</div>
						<!-- </form> -->
						<?= form_close(); ?>
					</div>
				</div>
			</div>
			<!--end col-->
		</div>
		<!--end row-->
	</div>
	<!--end container-->
</section>
<!--end section-->
<!-- Hero End -->
<?php $this->load->view('frontend/inc/foot_html'); ?>