<?php $this->load->view('frontend/inc/head_html'); ?>

<!-- Loader -->
<div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="double-bounce1"></div>
			<div class="double-bounce2"></div>
		</div>
	</div>
</div>
<!-- Loader -->

<div class="back-to-home rounded d-none d-sm-block">
	<a href="<?= base_url(); ?>" class="btn btn-icon btn-soft-primary"><i data-feather="home" class="icons"></i></a>
</div>

<!-- Hero Start -->
<section class="bg-home d-flex align-items-center">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-7 col-md-6">
				<div class="mr-lg-5">
					<img src="<?= base_url(); ?>assets/images/user/recovery.svg" class="img-fluid d-block mx-auto" alt="">
				</div>
			</div>
			<div class="col-lg-5 col-md-6">
				<div class="card login_page shadow rounded border-0">
					<div class="card-body">
						<?php
						$notif = $this->session->flashdata("notifikasi");
						if (!empty($notif)) {
							echo get_notif($notif['status'], $notif['pesan']);
						}
						?>
						<h4 class="card-title text-center">Lupa Katasandi</h4>
						<?= form_open('', [
							'class' => 'login-form mt-4',
							'autocomplete' => 'off'
						]); ?>
						<!-- <form class="login-form mt-4" method="POST" autocomplete="off"> -->
						<div class="row">
							<div class="col-lg-12">
								<p class="text-muted">Please enter your email address. You will receive a link to create a new password via email.</p>
								<div class="form-group position-relative">
									<label>Email address <span class="text-danger">*</span></label>
									<i data-feather="mail" class="fea icon-sm icons"></i>
									<input type="email" class="form-control pl-5 <?= (form_error('email')) ? 'is-invalid' : ''; ?>" id="email" placeholder="name@example.com" name="email" value="<?php echo set_value('email', ''); ?>" required="" autofocus>
									<?php echo form_error('email'); ?>
								</div>
							</div>
							<div class="col-lg-12">
								<button type="submit" name="lupa_password" value="lupa_password" class="btn btn-primary btn-block">Send</button>
							</div>
							<div class="mx-auto">
								<p class="mb-0 mt-3"><small class="text-dark mr-2">Remember your password ?</small> <a href="<?= base_url("login"); ?>" class="text-dark font-weight-bold">Sign in</a></p>
							</div>
						</div>
						<?= form_close(); ?>
						<!-- </form> -->
					</div>
				</div>
			</div>
			<!--end col-->
		</div>
		<!--end row-->
	</div>
	<!--end container-->
</section>
<!--end section-->
<!-- Hero End -->

<?php $this->load->view('frontend/inc/foot_html'); ?>