<?php $this->load->view('frontend/inc/head_html'); ?>

<div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="double-bounce1"></div>
			<div class="double-bounce2"></div>
		</div>
	</div>
</div>

<div class="back-to-home rounded d-none d-sm-block">
	<a href="<?= base_url(); ?>" class="btn btn-icon btn-soft-primary"><i data-feather="home" class="icons"></i></a>
</div>

<!-- Hero Start -->
<section class="bg-home d-flex align-items-center">
	<div class="container">
		<div class="row align-items-center">

			<?php if ($this->session->flashdata("notifikasi")) { ?>
				<div class="col-12 alert alert-<?= $this->session->flashdata("notifikasi")["status"] ?> alert-dismissible fade show" role="alert">
					<?= $this->session->flashdata("notifikasi")["msg"]; ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true"> × </span>
					</button>
				</div>
			<?php } ?>

			<div class="col-md-12 text-center mb-2">
				<h4 class="border-bottom border-primary">Invoice</h4>
			</div>

			<div class="col-lg-4 col-md-4 col-12 mt-4 pt-2">
				<div class="media align-items-center features">
					<div class="icons m-0 rounded h2 text-primary text-center px-3">
						<i class="uil uil-shopping-basket"></i>
					</div>
					<div class="content ml-4">
						<h5 class="mb-1">Kode Transaksi</h5>
						<p class="text-muted mb-0"><?= $data->kode_transaksi; ?></p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-12 mt-4 pt-2">
				<div class="media align-items-center features">
					<div class="icons m-0 rounded h2 text-primary text-center px-3">
						<i class="uil uil-list-ul"></i>
					</div>
					<div class="content ml-4">
						<h5 class="mb-1">Paket</h5>
						<p class="text-muted mb-0"><?= $paket->nama_paket; ?></p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-12 mt-4 pt-2">
				<div class="media align-items-center features">
					<div class="icons m-0 rounded h2 text-primary text-center px-3">
						<i class="uil uil-money-bill"></i>
					</div>
					<div class="content ml-4">
						<h5 class="mb-1">Tagihan</h5>
						<?php if (!empty($promo)) { ?>
							<p class="text-muted text mb-0">
								<del class="mr-2">
									<?= rupiah($paket->harga); ?>
								</del>
								<?php
								if ($promo->persen == "1") {
									$potongan 	= ($promo->potongan / 100) * $paket->harga;
									$harga		= $paket->harga - $potongan;
								} else {
									$harga		= $paket->harga - $promo->potongan;
								}
								echo rupiah($harga)
								?>
							</p>
						<?php } else {
							$harga = $paket->harga;
						?>
							<p class="text-muted mb-0"><?= rupiah($harga); ?></p>
						<?php } ?>
					</div>
				</div>
			</div>
			<!--end col-->

			<?php if ($data->paket_id == "1") {
				echo '<div class="col-md-12 mt-4"><div class="alert alert-info h1">Kami akan menghubungi anda.</div></div>';
			} else { ?>
				<div class="col-md-6 mt-4">
					<h5>Pemesan</h5>
					<ul class="list-group list-group-flush mb-1">
						<li class="list-group-item"><?= $data->nama; ?></li>
						<li class="list-group-item"><?= $data->email; ?></li>
						<li class="list-group-item"><?= $data->no_telp; ?></li>
					</ul>

					<p class="alert alert-info">Silahkan simpan kode transaksi anda. Untuk mengecek status pembayaran klik menu cek tagihan di halaman utama.</p>

					<?php if (strtotime($data->batas_bayar) < strtotime(date("Y-m-d"))) {
						echo '<div class="alert alert-info">Pembayaran anda sedang diproses.</div>';
					} else { ?>

						<?php if (!empty($bayar) && $bayar->status == "1") { ?>
							<div class="alert alert-info">Pembayaran anda sedang diproses.</div>
						<?php } elseif (!empty($bayar) && $bayar->status == "0") { ?>
							<div class="alert alert-danger">Pembayaran anda ditolak. Silahkan Unggah Ulang Bukti Pembayaran</div>
							<a href="javascript:void(0)" data-toggle="modal" data-target="#uploadForm" class="btn btn-primary">Upload Bukti Bayar</a>
						<?php } elseif (!empty($bayar) && $bayar->status == "2") { ?>
							<div class="alert alert-success">Pembayaran anda disetujui. Silahkan cek email anda. Terima Kasih.</div>
						<?php } else { ?>
							<a href="javascript:void(0)" data-toggle="modal" data-target="#uploadForm" class="btn btn-primary">Upload Bukti Bayar</a>
						<?php } ?>
					<?php } ?>
					<div class="modal fade" id="uploadForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content rounded shadow border-0">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalCenterTitle">Upload Bukti Bayar </h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<!-- <form action="<?= base_url("tagihan/bayar"); ?>" method="POST" enctype="multipart/form-data"> -->
								<?= form_open(base_url('tagihan/bayar'), [
									'enctype' => 'multipart/form-data'
								]); ?>
								<input type="hidden" name="kode_transaksi" value="<?= $data->kode_transaksi; ?>">
								<input type="hidden" name="tagihan" value="<?= $harga; ?>">
								<div class="modal-body">
									<div class="bg-white p-3 rounded box-shadow">
										<div class="form-group">
											<label for="rekening">Bank Pengirim</label>
											<select class="form-control <?= (form_error('bank_pengirim')) ? 'is-invalid' : ''; ?>" name="bank_pengirim" id="rekening">
												<option value="0">- Pilih -</option>
												<option <?= set_select('bank_pengirim', 'BNI'); ?> value="BNI">BNI</option>
												<option <?= set_select('bank_pengirim', 'BRI'); ?> value="BRI">BRI</option>
												<option <?= set_select('bank_pengirim', 'BCA'); ?> value="BCA">BCA</option>
												<option <?= set_select('bank_pengirim', 'MANDIRI'); ?> value="MANDIRI">MANDIRI</option>
												<option <?= set_select('bank_pengirim', 'BTN'); ?> value="BTN">BTN</option>
												<option <?= set_select('bank_pengirim', 'SULUTGO'); ?> value="SULUTGO">SULUTGO</option>
											</select>
											<?php echo form_error('bank_pengirim'); ?>
										</div>
										<div class="form-group">
											<label for="nama_pengirim">Nama Pengirim</label>
											<input type="text" class="form-control <?= (form_error('nama_pengirim')) ? 'is-invalid' : ''; ?>" name="nama_pengirim" id="nama_pengirim" placeholder="Nama Pengirim">
											<?php echo form_error('nama_pengirim'); ?>
										</div>
										<div class="form-group">
											<label for="nominal">Nominal</label>
											<input type="text" class="form-control <?= (form_error('nominal')) ? 'is-invalid' : ''; ?>" name="nominal" id="nominal" placeholder="Masukkan Nominal Transfer">
											<?php echo form_error('nominal'); ?>
										</div>
										<div class="form-group">
											<label for="rekening">Rekening Tujuan</label>
											<select class="form-control" name="bank_tujuan" id="rekening">
												<option <?= set_select('bank_tujuan', 'BNI', true); ?> value="BNI">BNI</option>
												<option <?= set_select('bank_tujuan', 'BRI'); ?> value="BRI">BRI</option>
											</select>
										</div>
										<div class="form-group">
											<label for="nominal">Bukti Bayar</label>
											<div class="custom-file">
												<input type="file" name="bukti_bayar" class="custom-file-input <?= (form_error('bukti_bayar')) ? 'is-invalid' : ''; ?>" id="customFile" onchange="previewLabel()" accept="image/x-png,image/jpeg">
												<label class="custom-file-label" for="customFile">Choose file</label>
												<?php echo form_error('bukti_bayar'); ?>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
									<button type="submit" class="btn btn-primary">Kirim</button>
								</div>
								<?= form_close(); ?>
								<!-- </form> -->
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 mt-4">
					<div class="alert alert-warning">Batas Pembayaran <?= tgl_indonesia($data->batas_bayar); ?></div>
					<div class="card bg-light rounded shadow border-0">
						<div class="card-body py-1">
							<img src="<?= base_url("assets/images/icon/payment.svg"); ?>" class="avatar avatar-md-sm" alt="">
							<div class="mt-4">
								<h5 class="card-title"><a href="javascript:void(0)" class="text-primary"> Rekening Kami</a></h5>
								<div class="row">
									<div class="col">
										<p class="text-muted mt-3 mb-0">
											<span class="text-dark d-block">BNI</span>
											No. 0379372795<br>
											an. Saronde Tech
										</p>
									</div>
									<div class="col">
										<p class="text-muted mt-3 mb-0">
											<span class="text-dark d-block">BRI</span>
											No. 7529-01-008274-53-7<br>
											an. CV SARONDE TECH
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
			<!--end col-->
		</div>
		<!--end row-->
	</div>
	<!--end container-->
</section>
<!--end section-->
<!-- Hero End -->
<script>
	// preview foto profil
	function previewLabel() {
		const foto = document.querySelector("#customFile");
		const fotoLabel = document.querySelector('.custom-file-label');
		fotoLabel.textContent = foto.files[0].name;
	}
</script>
<?php $this->load->view('frontend/inc/foot_html'); ?>