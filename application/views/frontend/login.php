<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Login Ngundang.in - Website dan Undangan Pemerintahan Digital</title>
    <script>
        const url = '<?php echo base_url(); ?>';
    </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- favicon -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/materialdesignicons.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/pe-icon-7.css">
    <!-- Magnific CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl.carousel.min.css">
    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl.theme.default.min.css">

    <!-- css -->
    <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/style.min.css" rel="stylesheet" type="text/css" />
    <!-- Favicon -->
    <style>
        * {
            margin: 0;
            padding: 0
        }

        html {
            scroll-behavior: smooth;
        }
    </style>
</head>

<body>
    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
    </div>


    <!--Navbar Start-->
    <nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-dark">
        <div class="container">
            <!-- LOGO -->
            <a class="navbar-brand logo" href="layout-one-1.html">
                <img src="<?= base_url(); ?>assets/images/event-logo.png" alt="" class="" height="90">
            </a>
            <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>

        </div>
    </nav>
    <!-- Navbar End -->
    <section class="hero-6-bg position-relative vh-100 d-flex align-items-center" id="home">

        <div class="container">
            <div class="row align-items-center">

                <div class="col-lg-6">

                    <div class="card login-page bg-white shadow rounded border-0">
                        <div class="card-body">

                            <div id="responseDiv">
                                <div id="message"></div>
                            </div>

                            <h4 class="card-title text-center">Login</h4>
                            <?= form_open('', [
                                'class' => 'login-form mt-4',
                                'id' => 'logForm',
                                'autocomplete' => 'on'
                            ]); ?>
                            <!-- <form class="login-form mt-4" method="POST" id="logForm" autocomplete="on"> -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group position-relative">
                                        <label>Your Email <span class="text-danger">*</span></label>

                                        <input type="email" class="form-control pl-5" placeholder="Email" name="email" id="email" required="" autofocus>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group position-relative">
                                        <label>Password <span class="text-danger">*</span></label>

                                        <input type="password" class="form-control pl-5" placeholder="Password" name="password" id="password" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 mb-0">

                                <button type="submit" class="btn btn-primary btn-block" id="logText">Masuk</button>
                            </div>

                        </div>
                        <?= form_close(); ?>
                        <!-- </form> -->
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mt-12 mt-lg-0">
                        <img src="http://toduwo.id/assets/images/user/login.svg" class="img-fluid d-block mx-auto" alt="">
                    </div>
                </div>

                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section-->
    <!-- Hero End -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/scrollspy.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.easing.min.js"></script>
    <!-- Counter js -->
    <script src="<?= base_url(); ?>assets/js/counter.int.js"></script>

    <!-- carousel -->
    <script src="<?= base_url(); ?>assets/js/owl.carousel.min.js"></script>
    <!-- Main Js -->
    <script src="<?= base_url(); ?>assets/js/login.js"></script>
    <script src="<?= base_url(); ?>assets/js/app.js"></script>



</body>

</html>