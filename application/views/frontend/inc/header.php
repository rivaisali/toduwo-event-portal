<!--Navbar Start-->
<nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-dark">
    <div class="container">
        <!-- LOGO -->
        <a class="navbar-brand logo" href="layout-one-1.html">
            <img src="<?= base_url(); ?>assets/images/event-logo.png" alt="" class="" height="90">
        </a>
        <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i class="mdi mdi-menu"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto navbar-center" id="mySidenav">
                <li class="nav-item active">
                    <a href="#home" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="#services" class="nav-link">Layanan</a>
                </li>
                <li class="nav-item">
                    <a href="#about" class="nav-link">Tentang Kami</a>
                </li>
                <li class="nav-item">
                    <a href="#features" class="nav-link">Fitur</a>
                </li>
                <li class="nav-item">
                    <a href="#clients" class="nav-link">Klien</a>
                </li>
            </ul>
            <div class="call-no">
                <a href="<?= base_url() ?>home" class="nav-link text-warning"><i class="mdi mdi-phone mr-1"></i> +62 87816994929</a>
            </div>
        </div>
    </div>
</nav>
<!-- Navbar End -->