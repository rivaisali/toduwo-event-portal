  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
  <script src="<?= base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url(); ?>assets/js/scrollspy.min.js"></script>
  <script src="<?= base_url(); ?>assets/js/jquery.easing.min.js"></script>
  <!-- Counter js -->
  <script src="<?= base_url(); ?>assets/js/counter.int.js"></script>
  <!--Particles JS-->
  <script src="<?= base_url(); ?>assets/js/particles.js"></script>
  <script src="<?= base_url(); ?>assets/js/particles.app.js"></script>
  <!-- carousel -->
  <script src="<?= base_url(); ?>assets/js/owl.carousel.min.js"></script>
  <!-- Main Js -->
  <script src="<?= base_url(); ?>assets/js/app.js"></script>
  </body>

  </html>