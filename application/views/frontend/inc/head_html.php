    <!doctype html>
    <html lang="zxx">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/materialdesignicons.min.css">
        <!-- Animate CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/pe-icon-7.css">
        <!-- Magnific CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl.carousel.min.css">
        <!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl.theme.default.min.css">

        <!-- css -->
        <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>assets/css/style.min.css" rel="stylesheet" type="text/css" />
        <!-- Favicon -->
        <!-- <link rel="icon" type="images/png" href="<?= base_url(); ?>assets/images/favicon.png"> -->
        <!-- Title -->
        <!-- <title>Toduwo.id</title> -->
        <link rel="shortcut icon" href="<?= base_url(); ?>assets_admin/img/logo-icon.png">
        <title>Ngundang.in - Solusi Undangan Digital untuk Event</title>
        <script>
            const url = '<?php echo base_url(); ?>';
        </script>
    </head>

    <body>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="dot1"></div>
                    <div class="dot2"></div>
                </div>
            </div>
        </div>