<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title; ?></title>
    <style>
        @font-face {
            font-family: 'Amagro-bold';
            src: url('<?= base_url("assets/font_sertifikat/Amagro-bold.ttf") ?>') format('truetype');
        }

        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
            background-image: url('uploads/background/<?= $format->background; ?>');
            background-position: center;
            background-size: cover;
            position: relative;
            text-align: center;
            background-repeat: no-repeat;
            width: 100%;
            height: 100%;
        }

        img.logo {
            margin-top: 25px;
            text-align: right;
            width: 100px;
        }

        .kop {
            /* border: 1px solid red; */
            height: 111px;
        }

        .kop h1 {
            color: #03757f;
            font-size: 50px;
            padding: 60px 0 0 0;
            /* margin-top: 50px; */
            letter-spacing: 10px;
        }

        .participation {
            display: block;
            margin-top: 50px;
            color: #fff;
            font-size: 25px;
            text-align: center;
            font-weight: bold;
            letter-spacing: 10px;
        }

        .box-judul {
            /* border: 1px solid yellow; */
            /* display: block; */
            width: 80%;
            margin: auto;
            text-align: center;
            /* margin: 0 auto; */
            /* height: 10px; */
        }

        .judul {
            text-align: center;
            display: block;
            margin-top: 30px;
            color: #000;
        }

        .tema {
            width: 100%;
            text-align: center;
            display: block;
            margin: 20px 0;
            color: #000;
            font-size: 30px;
            font-weight: bold;
        }

        .box-nama {
            /* border: 1px solid black; */
            height: 75px;
        }

        .nama {
            margin-top: 153px;
            display: block;
            color: #000;
            font-size: 30px;
            text-align: center;
            font-weight: bold;
        }

        .footer {
            text-align: center;
            display: block;
            color: #000;
            margin-top: 10px;
            height: 70px;
            /* background-color: red; */
        }

        .tanggal {
            text-align: center;
            display: block;
            color: #000;
            font-size: 25px;
            font-weight: bold;
        }

        .ttd {
            margin-top: 20px;
            display: block;
            color: #000;
        }

        .penyelenggara {
            font-size: 15px;
            /* margin: 0 auto; */
            /* line-height: 50px; */
            display: block;
            /* padding-top: 50px; */
            text-align: center;
        }

        .qr {
            position: absolute;
            bottom: 30;
            right: 25;
        }

        .qr img {
            width: 100px;
        }
    </style>
</head>

<body>
    <!-- <img src="<?= $base64_logo ?>" alt="logo" class="logo"> -->
    <div class="kop">
        <h1></h1>
    </div>

    <div class="box-partisipasi">
        <span class="participation">

        </span>
    </div>
    <div class="box-nama">
        <span class="nama">
            <?= $data['data']->nama_lengkap; ?>
        </span>
    </div>
    <!-- <div class="box-judul">
        <span class="judul">
            atas partisipasinya sebagai <b>PESERTA</b> pada kegiatan
        </span>
        <span class="tema">
            "<?= $data['data']->judul; ?>"
        </span>
    </div>
    <div class="box-footer">
        <span class="footer">
            yang diselenggarakan oleh Kementerian Komunikasi dan Informatika Republik Indonesia<br>
            dan Gerakan Nasional Literasi Digital di 34 Provinsi <br>
            dan 514 Kabupaten/Kota
        </span>
        <span class="tanggal">
            <?= tgl_indonesia($data['data']->tanggal); ?>
        </span>
    </div>
    <span class="ttd">
        <span class="penyelenggara">
        </span>
    </span> -->
    <div class="qr">
        <img src="uploads/qrcode/<?= $data['data']->undangan_id . '.png' ?>" alt="qrcode">
    </div>
</body>

</html>