<?php

class Format_sertifikat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Singapore');
        if (!$this->session->userdata('user')) {
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect('/login');
        }
    }

    private $base = 'format';
    private $folder = 'format_sertifikat';

    public function index()
    {

        $data['title'] = "Format Sertifikat";
        $data['page'] = $this->folder . "/index";
        $data["data"] = $this->crud_model->select_all_where("format_sertifikat", "user_id", user("user_id"));
        $data['base'] = $this->base;

        $this->load->view("backend/main", $data);
    }

    public function tambah()
    {
        if ($this->form_validation->run("format_sertifikat") == false) {
            $data['title'] = "Tambah Format Sertifikat";
            $data['page'] = $this->folder . "/tambah";
            $data['base'] = $this->base;
            $this->load->view("backend/main", $data);
        } else {
            $data = [
                "format_sertifikat_id" => $this->crud_model->cek_id("format_sertifikat", "format_sertifikat_id"),
                "judul" => $this->input->post("judul", true),
                // "keterangan" => $this->input->post("keterangan", true),
                // "footer" => $this->input->post("footer", true),
                "user_id" => user("user_id"),
            ];

            // $config['upload_path'] = './uploads/logo/';
            // $config['allowed_types'] = 'png';
            // $config['max_size'] = 2000;
            // $config['max_filename'] = '255';
            // $config['encrypt_name'] = true;

            // $this->load->library('upload', $config);
            // if ($this->upload->do_upload('logo')) {
            //     $data_file = $this->upload->data();
            //     $data['logo'] = $data_file['file_name'];
            // }

            $config['upload_path'] = './uploads/background/';
            $config['allowed_types'] = 'jpg';
            $config['max_size'] = 2000;
            $config['max_filename'] = '255';
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);
            if ($this->upload->do_upload('background')) {
                $data_file = $this->upload->data();
                $data['background'] = $data_file['file_name'];
            }

            $simpan = $this->crud_model->insert("format_sertifikat", $data);
            if ($simpan) {
                $notifikasi = array(
                    "status" => "success", "msg" => "Format berhasil ditambah",
                );
            } else {
                $notifikasi = array(
                    "status" => "danger", "msg" => "Format gagal ditambah",
                );
            }
            $this->session->set_flashdata("notifikasi", $notifikasi);
            redirect($this->base);
        }
    }

    // ubah
    public function ubah($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("format_sertifikat", ["format_sertifikat_id" => $id]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                if ($this->form_validation->run("format_sertifikat") == false) {
                    $data['data'] = $this->crud_model->select_one("format_sertifikat", "format_sertifikat_id", $id);
                    $data['title'] = "Ubah Format Sertifikat";
                    $data['page'] = $this->folder . "/ubah";
                    $data['base'] = $this->base;
                    $this->load->view("backend/main", $data);
                } else {
                    $data = [
                        "judul" => $this->input->post("judul", true),
                        // "keterangan" => $this->input->post("keterangan", true),
                        // "footer" => $this->input->post("footer", true),
                    ];

                    // $config['upload_path'] = './uploads/logo/';
                    // $config['allowed_types'] = 'png';
                    // $config['max_size'] = 2000;
                    // $config['max_filename'] = '255';
                    // $config['encrypt_name'] = true;

                    // $this->load->library('upload', $config);
                    // if ($this->upload->do_upload('logo')) {
                    //     $row    =   $this->crud_model->select_one("format_sertifikat", "format_sertifikat_id", $this->input->post('id', true));
                    //     unlink('./uploads/logo/' . $row->logo);
                    //     $data_file = $this->upload->data();
                    //     $data['logo'] = $data_file['file_name'];
                    // }

                    $config['upload_path'] = './uploads/background/';
                    $config['allowed_types'] = 'jpg';
                    $config['max_size'] = 2000;
                    $config['max_filename'] = '255';
                    $config['encrypt_name'] = true;

                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('background')) {
                        $row    =   $this->crud_model->select_one("format_sertifikat", "format_sertifikat_id", $this->input->post('id', true));
                        unlink('./uploads/background/' . $row->background);
                        $data_file = $this->upload->data();
                        $data['background'] = $data_file['file_name'];
                    }


                    $simpan = $this->crud_model->update("format_sertifikat", $data, "format_sertifikat_id", $this->input->post("id"));
                    if ($simpan) {
                        $notifikasi = array(
                            "status" => "success", "msg" => "Format berhasil diubah",
                        );
                    } else {
                        $notifikasi = array(
                            "status" => "danger", "msg" => "Format gagal diubah",
                        );
                    }
                    $this->session->set_flashdata("notifikasi", $notifikasi);
                    redirect($this->base);
                }
            }
        }
    }

    // hapus
    public function hapus($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("format_sertifikat", ["format_sertifikat_id" => $id]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                $hapus = $this->crud_model->hapus_id("format_sertifikat", "format_sertifikat_id", $id);
                if ($hapus) {
                    $notifikasi = array(
                        "status" => "success", "msg" => "Format berhasil dihapus",
                    );
                } else {
                    $notifikasi = array(
                        "status" => "danger", "msg" => "Format gagal diubah",
                    );
                }
                $this->session->set_flashdata("notifikasi", $notifikasi);
                redirect($this->base);
            }
        }
    }

    // cek file
    public function check_file($str, $name)
    {
        if ($this->input->post('id')) { // jika edit
            if ($name == "background") {
                $allowed_mime_type_arr  =   ['image/jpg', 'image/jpeg'];
            } else {
                $allowed_mime_type_arr = array('image/png');
            }
            if (isset($_FILES[$name]['name']) && $_FILES[$name]['name'] != "") {
                $mime = get_mime_by_extension($_FILES[$name]['name']);
                if (in_array($mime, $allowed_mime_type_arr)) {
                    if ($_FILES[$name]['size'] > '2000000') {
                        $this->form_validation->set_message('check_file', 'Maksimal 2MB');
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    $this->form_validation->set_message('check_file', ($name == "background") ? 'Background Harus JPG' : 'Logo Harus PNG');
                    return false;
                }
            } else {
                return true;
            }
        } else { // jika tambah
            if ($name == "background") {
                $allowed_mime_type_arr  =   ['image/jpg', 'image/jpeg'];
            } else {
                $allowed_mime_type_arr = array('image/png');
            }
            if (isset($_FILES[$name]['name']) && $_FILES[$name]['name'] != "") {
                $mime = get_mime_by_extension($_FILES[$name]['name']);
                if (in_array($mime, $allowed_mime_type_arr)) {
                    if ($_FILES[$name]['size'] > '2000000') {
                        $this->form_validation->set_message('check_file', 'Maksimal 2MB');
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    $this->form_validation->set_message('check_file', ($name == "background") ? 'Background Harus JPG' : 'Logo Harus PNG');
                    return false;
                }
            } else {
                $this->form_validation->set_message('check_file', 'File belum dipilih');
                return false;
            }
        }
    }

    // cek tampilan sertifikat
    public function cek($id = null)
    {
        if ($id === null) {
            $data['title']    =    "Halaman Tidak Ditemukan";
            $this->load->view('e404', $data);
        } else {
            $data = array(
                'title' => "Cek Tampilan Sertifikat",
                'format' => $this->crud_model->select_one("format_sertifikat", "format_sertifikat_id", $id)
            );
            generate_pdf("CEK-SERTIFIKAT.pdf", "cek_sertifikat.php", $data);
        }
    }
}
