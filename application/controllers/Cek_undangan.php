<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Cek_undangan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
    }

    // fungsi cek data undangan
    public function data($id = null)
    {
        if ($id === null) {
            $data['title']    =    "Halaman Tidak Ditemukan";
            $this->load->view('e404', $data);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id]);
            if ($cek_data) {
                $data['title']    =    "Halaman Tidak Ditemukan";
                $this->load->view('e404', $data);
            } else {
                $data['undangan']   =   $this->crud_model->select_one("undangan", "undangan_id", $id);
                $this->load->view("backend/undangan/cek_data_undangan", $data);
            }
        }
    }

    public function index($id = null)
    {
        if ($id === null) {
            $data['title']    =    "Halaman Tidak Ditemukan";
            $this->load->view('e404', $data);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id]);
            if ($cek_data) {
                $data['title']    =    "Halaman Tidak Ditemukan";
                $this->load->view('e404', $data);
            } else {
                $undangan = $this->crud_model->select_one("undangan", "undangan_id", $id);
                if ($undangan->jenis == "offline") {
                    $data['title']    =    "Undangan Tidak Ditemukan";
                    $data['msg']    =    "Undangan Tidak Ditemukan";
                    $this->load->view('e404', $data);
                } else {
                    $a = new DateTime();
                    $b = new DateTime($undangan->tanggal_selesai . " " . $undangan->jam_selesai);

                    $diff = $a->diff($b);
                    if ($diff->invert === 1) {
                        $data['title']    =    "Undangan Tidak Ditemukan";
                        $data['msg']    =    "Kegiatan Telah Selesai";
                        $this->load->view('e404', $data);
                    } else {
                        // if ($this->form_validation->run("tamu_registrasi_online") == false) {
                        if ($this->form_validation->run("jenis_form_" . $undangan->jenis_form) == false) {
                            // echo validation_errors();
                            $data['undangan']   =   $this->crud_model->select_one("undangan", "undangan_id", $id);
                            $data['propinsi']   =   $this->crud_model->select_custom("SELECT DISTINCT(namapropinsi) FROM wilayah");
                            // $data['kota']   =   $this->crud_model->select_custom("SELECT kodekabkota, namakabkota FROM wilayah group by kodekabkota, namakabkota");
                            // $data['kecamatan']   =   $this->crud_model->select_custom("SELECT namakecamatan FROM wilayah group by namakecamatan");
                            // $data['kelurahan']   =   $this->crud_model->select_custom("SELECT kelurahan FROM wilayah group by kelurahan");
                            // print_r($data['kecamatan']);
                            $this->load->view("backend/undangan/cek_undangan", $data);
                        } else {
                            $cek_tamu   =   $this->crud_model->select_one_where_array("tamu", ["no_telp" => $this->input->post('no_telp', true), "user_id" => $undangan->user_id]);
                            if (empty($cek_tamu)) {
                                $data = [
                                    "tamu_id" => $this->crud_model->cek_id("tamu", "tamu_id"),
                                    "nama_lengkap" => strtoupper($this->input->post("nama", true)),
                                    "nama_panggilan" => strtoupper($this->input->post("nama_panggilan", true)),
                                    // "alamat" => $this->input->post("alamat", true),
                                    "email" => $this->input->post("email", true),
                                    "no_telp" => $this->input->post("no_telp", true),
                                    "jenis_kelamin" => $this->input->post('jenis_kelamin', true),
                                    "pendidikan" => $this->input->post('pendidikan', true),
                                    "pekerjaan" => $this->input->post('pekerjaan', true),
                                    "usia" => $this->input->post('usia', true),
                                    "propinsi" => $this->input->post('propinsi', true),
                                    "kota" => $this->input->post('kota', true),
                                    // "kecamatan" => $this->input->post('kecamatan', true),
                                    // "kelurahan" => $this->input->post('kelurahan', true),
                                    "instansi" => $this->input->post("instansi", true),
                                    // "jabatan" => $this->input->post("jabatan", true),
                                    "user_id" => $undangan->user_id,
                                ];

                                if ($undangan->jenis_form == "2") {
                                    $data['alamat'] =   $this->input->post("alamat", true);
                                    $data['kecamatan'] =   $this->input->post("kecamatan", true);
                                    $data['kelurahan'] =   $this->input->post("kelurahan", true);
                                }

                                $simpan = $this->crud_model->insert("tamu", $data);
                                if ($simpan) {
                                    $data_tamu  =   [
                                        "tamu_undangan_id"  =>  generateRandomString("10", true, true),
                                        "undangan_id"   =>  $this->input->post("undangan_id"),
                                        "tamu_id"   =>  $data["tamu_id"],
                                        "lihat_undangan"    =>  "1",
                                        "status"    =>  "0",
                                        "dihadiri"  =>  $data['tamu_id'],
                                    ];
                                    $this->crud_model->insert("tamu_undangan", $data_tamu);

                                    // buat qrcode
                                    $this->load->library("barcode");
                                    $this->barcode->createQrCode($data_tamu["tamu_undangan_id"]);

                                    // kirim undangan
                                    $undangan = $this->crud_model->select_one("undangan", "undangan_id", $this->input->post("undangan_id"));

                                    $this->load->library("whatsapp");
                                    $pesan = "Selamat *" . $data["nama_lengkap"] . "* \n\n";
                                    $pesan .= "Kamu telah terdaftar pada kegiatan *$undangan->judul*\nyang diselenggarakan oleh ICT Watch, WhatsApp dan Kementerian Kominfo\n\n";
                                    $pesan .= "yang akan dilaksanakan pada : \n";
                                    $pesan .= "Hari : *" . hari($undangan->tanggal) . "* \n";
                                    $pesan .= "Tanggal : *" . tgl_indonesia($undangan->tanggal) . "* \n";
                                    $pesan .= "Jam : *" . jam($undangan->tanggal . " " . $undangan->jam) . " - " . jam($undangan->tanggal_selesai . " " . $undangan->jam_selesai) . "* \n";
                                    $pesan .= "Tempat : *" . $undangan->nama_lokasi . "*\n\n";
                                    $pesan .= $undangan->keterangan;
                                    // $pesan .= "Link Zoom Webinar : \n";
                                    // $pesan .= "https://us06web.zoom.us/j/86089782382?pwd=S0wwelEzZG90cy94RWErTy84dnVwQT09 \n";
                                    // $pesan .= "Webinar ID: 860 8978 2382 \n";
                                    // $pesan .= "Webinar Passcode: 491928 \n\n";
                                    // $pesan .= "Catat tanggalnya, jangan sampai ketinggalan";
                                    // $pesan = "Assalam alaikum Wr.Wb.\n";
                                    // $pesan .= "Kepada Yth *" . $data["nama_lengkap"] . "* \n";
                                    // $pesan .= "Terima Kasih Sudah Melakukan Pendaftaran pada Kegiatan *" . $undangan->judul . "* yang akan dilaksanakan pada :\n";
                                    // $pesan .= "Hari : *" . hari($undangan->tanggal) . "*\n";
                                    // $pesan .= "Tanggal : *" . tgl_indonesia($undangan->tanggal) . "*\n";
                                    // $pesan .= "Jam : *" . jam($undangan->tanggal . " " . $undangan->jam) . "*\n";
                                    // $pesan .= "Tempat : *" . $undangan->nama_lokasi . "*\n";
                                    // $pesan .= "Keterangan : *" . $undangan->keterangan . "*\n";
                                    $this->whatsapp->sendMessage($data["no_telp"], $pesan);

                                    // $pesan2 = "Gunakan Kode ini : *" . $data_tamu['tamu_undangan_id'] . "* untuk Registrasi Absen dan Cetak Sertifikat";
                                    $pesan2 = "Simpan dan Gunakan Kode : *" . $data_tamu['tamu_undangan_id'] . "* untuk *Registrasi Absen* dan *Cetak Sertifikat*";

                                    $this->whatsapp->sendMessage($data["no_telp"], $pesan2);
                                    // $this->whatsapp->sendImage($data["no_telp"], "Tunjukkan Code Ini Pada Penerima Tamu", base_url("uploads/qrcode/" . $data_tamu["tamu_undangan_id"] . ".png"));

                                    $notifikasi = array(
                                        "status" => "success", "msg" => "Terima Kasih Sudah Mendaftar",
                                    );
                                } else {
                                    $notifikasi = array(
                                        "status" => "danger", "msg" => "Gagal Mendaftar",
                                    );
                                }
                            } else {
                                $data = [
                                    "nama_lengkap" => strtoupper($this->input->post("nama", true)),
                                    "nama_panggilan" => strtoupper($this->input->post("nama_panggilan", true)),
                                    "email" => $this->input->post("email", true),
                                    "no_telp" => $this->input->post("no_telp", true),
                                    "jenis_kelamin" => $this->input->post('jenis_kelamin', true),
                                    "pendidikan" => $this->input->post('pendidikan', true),
                                    "pekerjaan" => $this->input->post('pekerjaan', true),
                                    "usia" => $this->input->post('usia', true),
                                    "propinsi" => $this->input->post('propinsi', true),
                                    "kota" => $this->input->post('kota', true),
                                    "instansi" => $this->input->post("instansi", true),
                                    "user_id" => $undangan->user_id,
                                ];

                                if ($undangan->jenis_form == "2") {
                                    $data['alamat'] =   $this->input->post("alamat", true);
                                    $data['kecamatan'] =   $this->input->post("kecamatan", true);
                                    $data['kelurahan'] =   $this->input->post("kelurahan", true);
                                }

                                $this->crud_model->update("tamu", $data, "tamu_id", $cek_tamu->tamu_id);

                                $data_tamu  =   [
                                    "tamu_undangan_id"  =>  generateRandomString("10", true, true),
                                    "undangan_id"   =>  $this->input->post("undangan_id"),
                                    "tamu_id"   =>  $cek_tamu->tamu_id,
                                    "lihat_undangan"    =>  "1",
                                    "status"    =>  "0",
                                    "dihadiri"  =>  $cek_tamu->tamu_id,
                                ];
                                $this->crud_model->insert("tamu_undangan", $data_tamu);

                                $this->load->library("barcode");
                                $this->barcode->createQrCode($data_tamu["tamu_undangan_id"]);

                                // kirim undangan
                                $undangan = $this->crud_model->select_one("undangan", "undangan_id", $this->input->post("undangan_id"));

                                $this->load->library("whatsapp");
                                // $pesan = "Assalam alaikum Wr.Wb.\n";
                                // $pesan .= "Kepada Yth *" . $cek_tamu->nama_lengkap . "* \n";
                                // $pesan .= "Terima Kasih Sudah Melakukan Pendaftaran pada Kegiatan *" . $undangan->judul . "* yang akan dilaksanakan pada :\n";
                                // $pesan .= "Hari : *" . hari($undangan->tanggal) . "*\n";
                                // $pesan .= "Tanggal : *" . tgl_indonesia($undangan->tanggal) . "*\n";
                                // $pesan .= "Jam : *" . jam($undangan->tanggal . " " . $undangan->jam) . "*\n";
                                // $pesan .= "Tempat : *" . $undangan->nama_lokasi . "*\n";
                                // $pesan .= "Keterangan : *" . $undangan->keterangan . "*\n";
                                $pesan = "Selamat *" . $cek_tamu->nama_lengkap . "* \n\n";
                                $pesan .= "Kamu telah terdaftar pada kegiatan *$undangan->judul*\n yang diselenggarakan oleh ICT Watch, WhatsApp dan Kementerian Kominfo\n\n";
                                $pesan .= "yang akan dilaksanakan pada : \n";
                                $pesan .= "Hari : *" . hari($undangan->tanggal) . "* \n";
                                $pesan .= "Tanggal : *" . tgl_indonesia($undangan->tanggal) . "* \n";
                                $pesan .= "Jam : *" . jam($undangan->tanggal . " " . $undangan->jam) . " - " . jam($undangan->tanggal_selesai . " " . $undangan->jam_selesai) . "* \n";
                                $pesan .= "Tempat : *" . $undangan->nama_lokasi . "*\n\n";
                                // $pesan .= "Link Zoom Webinar : \n";
                                // $pesan .= "https://us06web.zoom.us/j/86089782382?pwd=S0wwelEzZG90cy94RWErTy84dnVwQT09 \n";
                                // $pesan .= "Webinar ID: 860 8978 2382 \n";
                                // $pesan .= "Webinar Passcode: 491928 \n\n";
                                // $pesan .= "Catat tanggalnya, jangan sampai ketinggalan";
                                $pesan .= $undangan->keterangan;
                                $this->whatsapp->sendMessage($cek_tamu->no_telp, $pesan);

                                // $pesan2 = "Gunakan Kode ini : *" . $data_tamu['tamu_undangan_id'] . "* untuk Registrasi Absen dan Cetak Sertifikat";
                                $pesan2 = "Simpan dan Gunakan Kode : *" . $data_tamu['tamu_undangan_id'] . "* untuk *Registrasi Absen* dan *Cetak Sertifikat*";

                                $this->whatsapp->sendMessage($cek_tamu->no_telp, $pesan2);
                                // $this->whatsapp->sendImage($data["no_telp"], "Tunjukkan Code Ini Pada Penerima Tamu", base_url("uploads/qrcode/" . $data_tamu["tamu_undangan_id"] . ".png"));

                                $notifikasi = array(
                                    "status" => "success", "msg" => "Terima Kasih Sudah Mendaftar",
                                );
                            }
                            $this->session->set_flashdata("notifikasi", $notifikasi);
                            redirect("undangan/" . $this->input->post("undangan_id"));
                        }
                    }
                }
            }
        }
    }

    // export peserta
    public function export_peserta($id = null)
    {
        if ($id === null) {
            $data['title']    =    "Halaman Tidak Ditemukan";
            $this->load->view('e404', $data);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id]);
            if ($cek_data) {
                $data['title']    =    "Halaman Tidak Ditemukan";
                $this->load->view('e404', $data);
            } else {
                $spreadsheet = new Spreadsheet();

                $styleTitle = [
                    'font'  => [
                        'bold'  => true,
                        'color' => ['rgb' => '000000'],
                        'size'  => 15,
                        'name'  => 'Verdana'
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ];

                $styleHeader = [
                    'font'  => [
                        'color' => ['rgb' => 'ffffff'],
                        'size'  => 12,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => '235ceb',
                        ]
                    ]
                ];

                $spreadsheet->getActiveSheet()->mergeCells('A1:P1');

                for ($i = 1; $i <= 16; $i++) {
                    $spreadsheet->getActiveSheet()->getColumnDimension(number_to_alphabet($i))->setAutoSize(true);
                }

                $this->load->model("join_model");
                // $undangan   =   $this->crud_model->select_one("undangan","")
                $data   =   $this->join_model->dua_tabel_where_array_order("tamu", "tamu_undangan", "tamu_id", ["tamu_undangan.undangan_id" => $id], "tamu_undangan.create_at", "ASC");

                $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleTitle);
                $spreadsheet->getActiveSheet()->getStyle('A3:P3')->applyFromArray($styleHeader);

                $sheet = $spreadsheet->getActiveSheet();
                $sheet->setCellValue('A1', "Peserta");
                $sheet->setCellValue('A3', 'No.');
                $sheet->setCellValue('B3', 'NAMA LENGKAP');
                $sheet->setCellValue('C3', 'NAMA PANGGILAN');
                $sheet->setCellValue('D3', 'EMAIL');
                $sheet->setCellValue('E3', 'NO TELP / WA');
                $sheet->setCellValue('F3', 'JENIS KELAMIN');
                $sheet->setCellValue('G3', 'USIA');
                $sheet->setCellValue('H3', 'PEKERJAAN');
                $sheet->setCellValue('I3', 'PENDIDIKAN');
                $sheet->setCellValue('J3', 'ORGANISASI / KOMUNITAS');
                $sheet->setCellValue('K3', 'ALAMAT');
                $sheet->setCellValue('L3', 'KELURAHAN');
                $sheet->setCellValue('M3', 'KECAMATAN');
                $sheet->setCellValue('N3', 'KAB / KOTA');
                $sheet->setCellValue('O3', 'PROPINSI');
                $sheet->setCellValue('P3', 'TGL DAFTAR');
                $sheet->setCellValue('Q3', 'KODE UNDANGAN');

                if (!empty($data)) {
                    $no = 1;
                    $x = 4;
                    foreach ($data as $row) {
                        $sheet->setCellValue('A' . $x, $no++);
                        $sheet->setCellValue('B' . $x, $row->nama_lengkap);
                        $sheet->setCellValue('C' . $x, $row->nama_panggilan);
                        $sheet->setCellValue('D' . $x, $row->email);
                        $sheet->setCellValue('E' . $x, $row->no_telp);
                        $sheet->setCellValue('F' . $x, $row->jenis_kelamin);
                        $sheet->setCellValue('G' . $x, $row->usia);
                        $sheet->setCellValue('H' . $x, $row->pekerjaan);
                        $sheet->setCellValue('I' . $x, $row->pendidikan);
                        $sheet->setCellValue('J' . $x, $row->instansi);
                        $sheet->setCellValue('K' . $x, $row->alamat);
                        $sheet->setCellValue('L' . $x, $row->kelurahan);
                        $sheet->setCellValue('M' . $x, $row->kecamatan);
                        $sheet->setCellValue('N' . $x, $row->kota);
                        $sheet->setCellValue('O' . $x, $row->propinsi);
                        $sheet->setCellValue('P' . $x, tgl_laporan($row->create_at) . " " . substr($row->create_at, 11, 5));
                        $sheet->setCellValue('Q' . $x, $row->tamu_undangan_id);
                        $x++;
                    }
                }

                $writer = new Xlsx($spreadsheet);
                $filename = "Peserta Seminar";

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                header('Cache-Control: max-age=0');

                $writer->save('php://output');
            }
        }
    }

    public function cari_kota_by_propinsi()
    {
        $propinsi = $this->input->post('propinsi', true);
        $data = $this->crud_model->select_custom("SELECT kodekabkota, namakabkota FROM wilayah where namapropinsi = '" . $propinsi . "' group by kodekabkota, namakabkota");
        $ret["data"]   =   $data;
        // $ret["data"]   =   ["oke"];

        echo json_encode($ret);
    }

    public function cari_kecamatan_by_kota()
    {
        $kota = str_replace_first(" ", "-", $this->input->post('kota', true));
        // $kota = ;
        $pisah  =   explode("-", $kota);
        $kode   =   $pisah[0];
        $nama   =   $pisah[1];
        $data = $this->crud_model->select_custom("SELECT namakecamatan FROM wilayah where namakabkota = '" . $nama . "' and kodekabkota = '" . $kode . "' group by namakecamatan");
        $ret["data"]   =   $data;
        // $ret["data"]   =   ["oke"];

        echo json_encode($ret);
    }

    public function cari_kelurahan_by_kecamatan()
    {
        $kecamatan  =   $this->input->post('kecamatan', true);
        $data = $this->crud_model->select_custom("SELECT kelurahan FROM wilayah where namakecamatan = '" . $kecamatan . "' group by kelurahan");
        $ret["data"]   =   $data;
        // $ret["data"]   =   ["oke"];

        echo json_encode($ret);
    }

    // cek nomor hp terdaftar di WA
    public function check_nomor($post_string)
    {
        if ($post_string == "") {
            $this->form_validation->set_message('check_nomor', '{field} Harus diisi');
            return false;
        } else {
            $this->load->library('whatsapp');
            $nomor = json_decode($this->whatsapp->checkNumber($post_string), true);
            if ($nomor["status"] === true) {
                $undangan   =   $this->crud_model->select_one('undangan', 'undangan_id', $this->input->post('undangan_id', true));
                $cek_tamu   =   $this->crud_model->select_one_where_array("tamu", ["no_telp" => $post_string, "user_id" => $undangan->user_id]);
                if (empty($cek_tamu)) {
                    return true;
                } else {
                    $cek_tamu_undangan   =   $this->crud_model->select_one_where_array("tamu_undangan", ["tamu_id" => $cek_tamu->tamu_id, "undangan_id" => $undangan->undangan_id]);
                    if (empty($cek_tamu_undangan)) {
                        return true;
                    } else {
                        $this->form_validation->set_message('check_nomor', "Nomor telah digunakan oleh $cek_tamu->nama_lengkap");
                        return false;
                    }
                }
            } else {
                $this->form_validation->set_message('check_nomor', 'Nomor Tidak terdaftar DI WA');
                return false;
            }
        }
    }

    // absen online
    public function absen_online($id = null)
    {
        if ($id === null) {
            $data['title']    =    "Halaman Tidak Ditemukan";
            $this->load->view('e404', $data);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id]);
            if ($cek_data) {
                $data['title']    =    "Halaman Tidak Ditemukan";
                $this->load->view('e404', $data);
            } else {
                $undangan = $this->crud_model->select_one("undangan", "undangan_id", $id);
                $a = new DateTime();
                // $b = new DateTime($undangan->tanggal_selesai . " " . $undangan->jam_selesai);
                $b = new DateTime($undangan->tanggal . " " . $undangan->jam);

                $diff = $a->diff($b);

                // print_r($diff);
                // if ($diff->invert === 1) {
                // echo 'Sudah Bisa';
                if ($undangan->absen == "tutup") {
                    $data['msg']    =    "Absen telah ditutup";
                    $this->load->view('e404', $data);
                } else {
                    $data['undangan']   =   $this->crud_model->select_one("undangan", "undangan_id", $id);
                    $this->load->view("backend/undangan/absen_sertifikat", $data);
                }
                // } else {
                // echo 'Belum Bisa';
                //     $data['msg']    =    "Belum bisa ambil absen";
                //     $this->load->view('e404', $data);
                // }
            }
        }
    }

    public function cek_kode_absen_online()
    {
        $id = $this->input->post('undangan_id', true);
        $kode = $this->input->post('kode', true);
        $undangan = $this->crud_model->select_one("undangan", "undangan_id", $id);
        $cek_tamu   =   $this->crud_model->select_one_where_array("tamu_undangan", [
            "undangan_id" => $id,
            "tamu_undangan_id" => $kode
        ]);
        if (empty($cek_tamu)) {
            $ret["status"]  =   0;
            $ret["msg"]     =   "Kode Tidak Dikenali";
        } else {
            $this->load->library("whatsapp");
            $pertanyaan =   $this->crud_model->select_all_where_array("pertanyaan_absen", [
                "paket" => $undangan->pertanyaan_absen,
                "user_id" => $undangan->user_id
            ]);
            if ($cek_tamu->absen_sertifikat == "1") {
                $ret["status"]  =   0;
                $ret["msg"]     =   "Anda telah melakukan absen";
            } else {
                $tamu   =   $this->crud_model->select_one("tamu", "tamu_id", $cek_tamu->tamu_id);
                $pesan = "Link cetak sertifikat : " . base_url("sertifikat/" . $kode);
                if (empty($pertanyaan)) {
                    $this->crud_model->update("tamu_undangan", ["status" => "1", "absen_sertifikat" => "1"], "tamu_undangan_id", $kode);
                    $this->crud_model->insert("check_in", [
                        "check_in_id"   =>  $this->crud_model->cek_id("check_in", "check_in_id"),
                        "tamu_undangan_id"  =>  $kode,
                        "tanggal_check_in"  =>  date("Y-m-d"),
                        "jam_check_in"  =>  date("H:i:s")
                    ]);
                    $ret["status"]  =   1;
                    $ret["msg"]     =   "Sertifikat telah dikirim ke nomor WA anda";
                    $this->whatsapp->sendMessage($tamu->no_telp, $pesan);
                } else {
                    if ($cek_tamu->absen_sertifikat == "1") {
                        $ret["status"]  =   1;
                        $ret["msg"]     =   "Sertifikat telah dikirim ke nomor WA anda";
                        $this->whatsapp->sendMessage($tamu->no_telp, $pesan);
                    } else {
                        $ret["status"]  =   1;
                        $ret["kode"]    =   $kode;
                        $ret["id"]      =   $id;
                        $ret["msg"]     =   "Silahkan jawab pertanyaan dibawah ini untuk mendapatkan link sertifikat";
                        $ret["pertanyaan"]  =   $pertanyaan;
                    }
                }
            }
        }
        $this->load->view("backend/undangan/response_absen_sertifikat", $ret);
    }

    public function jawab_pertanyaan()
    {
        // print_r($this->input->post());
        $this->load->library("whatsapp");
        $data   =   [];
        $undangan_id    =   $this->input->post('undangan_id', true);
        $kode    =   $this->input->post('kode', true);
        $pertanyaan    =   $this->input->post('pertanyaan', true);
        $jawaban    =   $this->input->post('jawaban', true);
        foreach ($jawaban as $ind => $val) {
            $data[$pertanyaan[$ind]]    =   $val;
        }
        $jawaban_absen  =   (json_encode($data));
        $update = $this->crud_model->update("tamu_undangan", ["jawaban_absen" => $jawaban_absen, "absen_sertifikat" => "1", "status" => "1"], "tamu_undangan_id", $kode);
        $this->load->model("join_model");
        $tamu   =   $this->join_model->dua_tabel_where_row("tamu", "tamu_undangan", "tamu_id", "tamu_undangan_id", $kode);
        if ($update) {
            $this->crud_model->insert("check_in", [
                "check_in_id"   =>  $this->crud_model->cek_id("check_in", "check_in_id"),
                "tamu_undangan_id"  =>  $kode,
                "tanggal_check_in"  =>  date("Y-m-d"),
                "jam_check_in"  =>  date("H:i:s")
            ]);
            $pesan = "Link cetak sertifikat : " . base_url("sertifikat/" . $kode);
            $this->whatsapp->sendMessage($tamu->no_telp, $pesan);
            $notifikasi = array(
                "status" => "success", "msg" => "Sertifikat telah dikirim ke nomor WA anda",
            );
        } else {
            $notifikasi = array(
                "status" => "danger", "msg" => "Gagal!",
            );
        }
        $this->session->set_flashdata("notifikasi", $notifikasi);
        redirect("absen/" . $undangan_id);
    }

    // cetak serfifikat
    public function cetak_sertifikat($id = null)
    {
        if ($id === null) {
            $data['title']    =    "Halaman Tidak Ditemukan";
            $this->load->view('e404', $data);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("tamu_undangan", ["tamu_undangan_id" => $id]);
            if ($cek_data) {
                $data['title']    =    "Halaman Tidak Ditemukan";
                $this->load->view('e404', $data);
            } else {
                $this->load->model("join_model");
                $undangan = $this->join_model->tiga_tabel_where_array_row("tamu_undangan", "undangan", "tamu", "undangan_id", "tamu_id", [
                    "tamu_undangan_id" => $id
                ]);
                if ($undangan->absen_sertifikat != "1") {
                    $data['title']    =    "";
                    $this->load->view('e404', $data);
                } else {
                    if ($undangan->cetak_sertifikat != "1") {
                        $this->crud_model->update("tamu_undangan", ["cetak_sertifikat" => "1"], "tamu_undangan_id", $id);
                    }
                    $data = array(
                        'title' => 'Sertifikat ' . $undangan->judul,
                        'data' => $undangan,
                        'format' => $this->crud_model->select_one("format_sertifikat", "format_sertifikat_id", $undangan->format_sertifikat_id)
                    );
                    // print_r($undangan);
                    generate_pdf("SERTIFIKAT.pdf", "cetak_sertifikat.php", $data);
                }
            }
        }
    }
}
