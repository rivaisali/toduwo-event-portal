<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Testing extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
    }

    public function index()
    {
        // $spreadsheet = new Spreadsheet();

        // $styleTitle = [
        //     'font'  => [
        //         'bold'  => true,
        //         'color' => ['rgb' => '000000'],
        //         'size'  => 15,
        //         'name'  => 'Verdana'
        //     ],
        //     'alignment' => [
        //         'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        //         'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
        //     ],
        // ];

        // $styleHeader = [
        //     'font'  => [
        //         'color' => ['rgb' => 'ffffff'],
        //         'size'  => 12,
        //     ],
        //     'alignment' => [
        //         'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        //         'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
        //     ],
        //     'fill' => [
        //         'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
        //         'startColor' => [
        //             'argb' => '235ceb',
        //         ]
        //     ]
        // ];

        // $spreadsheet->getActiveSheet()->mergeCells('A1:I1');
        // // $spreadsheet->getActiveSheet()->getStyle('C4')->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);

        // $this->load->model("join_model");
        // $data   =   $this->join_model->dua_tabel_where_array_order("tamu", "tamu_undangan", "tamu_id", ["tamu_undangan.undangan_id" => "5zfwlM5JMF"], "tamu_undangan.create_at", "ASC");

        // $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleTitle);
        // $spreadsheet->getActiveSheet()->getStyle('A3:I3')->applyFromArray($styleHeader);

        // $sheet = $spreadsheet->getActiveSheet();
        // $sheet->setCellValue('A1', "Peserta");
        // $sheet->setCellValue('A3', 'No.');
        // $sheet->setCellValue('B3', 'NAMA');
        // $sheet->setCellValue('C3', 'NO TELP / WA');
        // $sheet->setCellValue('D3', 'ALAMAT');
        // $sheet->setCellValue('E3', 'KELURAHAN');
        // $sheet->setCellValue('F3', 'KECAMATAN');
        // $sheet->setCellValue('G3', 'KAB / KOTA');
        // $sheet->setCellValue('H3', 'PROPINSI');
        // $sheet->setCellValue('I3', 'TGL DAFTAR');

        // if (!empty($data)) {
        //     $no = 1;
        //     $x = 4;
        //     foreach ($data as $row) {
        //         $sheet->setCellValue('A' . $x, $no++);
        //         $sheet->setCellValue('B' . $x, $row->nama_lengkap);
        //         $sheet->setCellValue('C' . $x, $row->no_telp);
        //         $sheet->setCellValue('D' . $x, $row->alamat);
        //         $sheet->setCellValue('E' . $x, $row->kelurahan);
        //         $sheet->setCellValue('F' . $x, $row->kecamatan);
        //         $sheet->setCellValue('G' . $x, $row->kota);
        //         $sheet->setCellValue('H' . $x, $row->propinsi);
        //         $sheet->setCellValue('I' . $x, tgl_laporan($row->create_at));
        //     }
        // }

        // $writer = new Xlsx($spreadsheet);
        // $filename = "Testing";

        // header('Content-Type: application/vnd.ms-excel');
        // header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        // header('Cache-Control: max-age=0');

        // $writer->save('php://output');
        // echo '1 jam kedepan: ' . date('H:i:s', time() - (60 * 120)) . '<br/>';
        $this->load->library('whatsapp');
        // $cek_nomor = json_decode($this->whatsapp->checkNumber('082347996363'), true);
        // $cek_nomor = json_decode($this->whatsapp->sendMessage('082347996363', 'tes'));
        // if ($cek_nomor["status"] === true) {
        //     echo "terdaftar";
        // } else {
        //     echo "tidak terdaftar";
        // }
        // $this->whatsapp->sendMessage('082347996363', 'tes');
        // echo "tes";
        // $dari = "2021-03-25"; // tanggal mulai
        // $sampai = "2021-04-02"; // tanggal akhir

        // while (strtotime($dari) <= strtotime($sampai)) {
        //     echo "$dari<br/>";
        //     $dari = date("Y-m-d", strtotime("+1 day", strtotime($dari))); //looping tambah 1 date
        // }
        $pesan = nl2br("Hello \n World");
        $kirim_pesan = $this->whatsapp->sendMessage("082347996363", $pesan);
        // $kirim_pesan = $this->whatsapp->checkNumber("082347996363");
        print_r(json_decode($kirim_pesan));
        // $data = array(
        //     'title' => 'Add PDF | ERP',
        //     'data' => 'Test'
        // );
        // $assets =   [
        //     "/assets/css/sertifikat.css",
        // ];
        // generate_pdf("welcome.pdf", "welcome.php", $data, $assets);
        // $this->load->view("welcome");
        // $a = new DateTime();
        // $b = new DateTime("2021-07-09 10:04:00");

        // $diff = $a->diff($b);

        // // print_r($diff);
        // if ($diff->invert === 1) {
        //     echo 'Sudah lewat';
        // } else {
        //     echo 'Masih Bisa';
        // }
        // echo generateRandomString("10", true, true);
    }
}
