<?php
class Buku_tamu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("join_model");
        date_default_timezone_set('Asia/Singapore');
    }

    private $base = 'buku_tamu';
    private $folder = 'buku_tamu';

    public function index($id)
    {
        $data['title'] = "Buat Undangan";
        $undangan = $this->crud_model->select_one("undangan", "undangan_id", $id);
        $data["data"] = $undangan;
        $this->load->library("barcode");
        $this->barcode->createQrCodeUndangan(base_url("registrasi_manual/" . $id), "regis-manual-" . $id);
        $this->load->view("bukutamu/index", $data);
    }

    public function tamu($id)
    {
        $term = $_GET['term'];

        $data = $this->join_model->dua_tabel_where_like("tamu_undangan", "tamu", "tamu_id", "undangan_id", $id, "nama_lengkap", $term);

        if (count($data) > 0) {
            foreach ($data as $tamu) {
                $json[] = array(
                    'label' => $tamu->tamu_undangan_id . "- " . $tamu->nama_lengkap,
                    'value' => $tamu->nama_lengkap,
                    'code' => $tamu->tamu_undangan_id, // text sugesti saat user mengetik di input box
                );
            }
        } else {
            $json[] = array(
                'label' => "Data tidak ditemukan",
                'value' => "Data tidak ditemukan",
            );
        }
        echo json_encode($json);
    }

    public function checkin($id)
    {

        $code = $this->input->post("code", true);

        $data = [
            "status" => "1",
        ];

        $undangan = $this->crud_model->select_one_where_array("tamu_undangan", ["tamu_undangan_id" => $code, "undangan_id" => $id]);
        if ($undangan) {
            $result = $this->crud_model->update("tamu_undangan", $data, "tamu_undangan_id", $code);
            $cek_checkin    =   $this->crud_model->select_one_where_array("check_in", [
                "tamu_undangan_id" =>   $code,
                "tanggal_check_in"   =>  date("Y-m-d")
            ]);
            if (empty($cek_checkin)) {
                $this->crud_model->insert("check_in", [
                    "check_in_id" => $this->crud_model->cek_id("check_in", "check_in_id"),
                    "tamu_undangan_id"  =>  $code,
                    "tanggal_check_in"  =>  date("Y-m-d"),
                    "jam_check_in"      =>  date("H:i:s")
                ]);
            }
            if ($result) {
                $tamu = $this->join_model->dua_tabel_where_array_row("tamu_undangan", "tamu", "tamu_id", ["tamu_undangan_id" => $code, "undangan_id" => $id]);
                if ($tamu->tamu_id == $tamu->dihadiri) {
                    $data['hasil'] = 'sukses';
                    $data['nama_lengkap'] = $tamu->nama_lengkap;
                } else {
                    $tamu_ = $this->join_model->dua_tabel_where_array_row("tamu_undangan", "tamu", "tamu_id", ["tamu_undangan.tamu_id" => $tamu->dihadiri, "undangan_id" => $id]);
                    $data['hasil'] = 'sukses';
                    $data['nama_lengkap'] = $tamu_->nama_lengkap;
                }
            } else {
                $data['error'] = 'Gagal Checkin';
            }
        } else {
            $data['error'] = 'Tamu tidak dikenal!';
        }
        echo json_encode($data);
    }
}
