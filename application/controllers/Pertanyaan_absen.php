<?php

class Pertanyaan_absen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Singapore');
        if (!$this->session->userdata('user')) {
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect('/login');
        }
    }

    private $base = 'pertanyaan';
    private $folder = 'pertanyaan_absen';

    public function index()
    {

        $data['title'] = "Pertanyaan Absen";
        $data['page'] = $this->folder . "/index";
        $data["data"] = $this->crud_model->select_all_where("pertanyaan_absen", "user_id", user("user_id"));
        $data['base'] = $this->base;

        $this->load->view("backend/main", $data);
    }

    public function tambah()
    {
        if ($this->form_validation->run("pertanyaan_absen") == false) {
            $data['title'] = "Tambah Pertanyaan Absen";
            $data['page'] = $this->folder . "/tambah";
            $data['base'] = $this->base;
            $this->load->view("backend/main", $data);
        } else {
            $data = [
                "pertanyaan_absen_id" => $this->crud_model->cek_id("pertanyaan_absen", "pertanyaan_absen_id"),
                "paket" => $this->input->post("paket", true),
                "pertanyaan" => $this->input->post("pertanyaan", true),
                "opsi_a" => $this->input->post("opsi_a", true),
                "opsi_b" => $this->input->post("opsi_b", true),
                "opsi_c" => $this->input->post("opsi_c", true),
                "opsi_d" => $this->input->post("opsi_d", true),
                "user_id" => user("user_id"),
            ];

            $simpan = $this->crud_model->insert("pertanyaan_absen", $data);
            if ($simpan) {
                $notifikasi = array(
                    "status" => "success", "msg" => "Pertanyaan berhasil ditambah",
                );
            } else {
                $notifikasi = array(
                    "status" => "danger", "msg" => "Pertanyaan gagal ditambah",
                );
            }
            $this->session->set_flashdata("notifikasi", $notifikasi);
            redirect($this->base);
        }
    }

    // ubah
    public function ubah($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("pertanyaan_absen", ["pertanyaan_absen_id" => $id]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                if ($this->form_validation->run("pertanyaan_absen") == false) {
                    $data['data'] = $this->crud_model->select_one("pertanyaan_absen", "pertanyaan_absen_id", $id);
                    $data['title'] = "Ubah Pertanyaan Absen";
                    $data['page'] = $this->folder . "/ubah";
                    $data['base'] = $this->base;
                    $this->load->view("backend/main", $data);
                } else {
                    $data = [
                        "paket" => $this->input->post("paket", true),
                        "pertanyaan" => $this->input->post("pertanyaan", true),
                        "opsi_a" => $this->input->post("opsi_a", true),
                        "opsi_b" => $this->input->post("opsi_b", true),
                        "opsi_c" => $this->input->post("opsi_c", true),
                        "opsi_d" => $this->input->post("opsi_d", true),
                    ];

                    $simpan = $this->crud_model->update("pertanyaan_absen", $data, "pertanyaan_absen_id", $this->input->post("id"));
                    if ($simpan) {
                        $notifikasi = array(
                            "status" => "success", "msg" => "Pertanyaan berhasil diubah",
                        );
                    } else {
                        $notifikasi = array(
                            "status" => "danger", "msg" => "Pertanyaan gagal diubah",
                        );
                    }
                    $this->session->set_flashdata("notifikasi", $notifikasi);
                    redirect($this->base);
                }
            }
        }
    }

    // hapus
    public function hapus($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("pertanyaan_absen", ["pertanyaan_absen_id" => $id]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                $hapus = $this->crud_model->hapus_id("pertanyaan_absen", "pertanyaan_absen_id", $id);
                if ($hapus) {
                    $notifikasi = array(
                        "status" => "success", "msg" => "Pertanyaan berhasil dihapus",
                    );
                } else {
                    $notifikasi = array(
                        "status" => "danger", "msg" => "Pertanyaan gagal diubah",
                    );
                }
                $this->session->set_flashdata("notifikasi", $notifikasi);
                redirect($this->base);
            }
        }
    }

    function get_autocomplete($term)
    {
        // if (isset($_GET['term'])) {
        $result = $this->crud_model->select_all_where_array("pertanyaan_absen", ["user_id" => user("user_id")], ["paket" => $term], ["pertanyaan_absen.paket"]);
        if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = [
                    'value' => $row->paket,
                    'paket' => $row->paket,
                ];
            echo json_encode($arr_result);
        }
        // }
    }
}
