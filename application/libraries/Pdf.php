<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 *
 * Convert HTML to PDF in CodeIgniter applications.
 *
 * @package            CodeIgniter
 * @subpackage        Libraries
 * @category        Libraries
 * @author            Hostmystory
 * @link            https://www.hostmystory.com
 */

// Dompdf namespace
use Dompdf\Dompdf;

class Pdf
{
    public function __construct()
    {
        // require_once autoloader 
        require_once dirname(__FILE__) . '/dompdf/autoload.inc.php';
        $pdf = new DOMPDF();
        $options = $pdf->getOptions();
        $options->setDefaultFont('Courier');
        $options->setChroot(FCPATH);
        $pdf->setOptions($options);
        $CI = &get_instance();
        $CI->dompdf = $pdf;
    }
}
