<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function konversi_harga($n, $precision = 1)
{
    if ($n < 900) {
        // 0 - 900
        $n_format = number_format($n, $precision);
        $suffix = '';
    } else if ($n < 900000) {
        // 0.9k-850k
        $n_format = number_format($n / 1000, $precision);
        $suffix = ' Rb';
    } else if ($n < 900000000) {
        // 0.9m-850m
        $n_format = number_format($n / 1000000, $precision);
        $suffix = ' Jt';
    } else if ($n < 900000000000) {
        // 0.9b-850b
        $n_format = number_format($n / 1000000000, $precision);
        $suffix = ' Ml';
    } else {
        // 0.9t+
        $n_format = number_format($n / 1000000000000, $precision);
        $suffix = ' Tr';
    }
    // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
    // Intentionally does not affect partials, eg "1.50" -> "1.50"
    if ($precision > 0) {
        $dotzero = '.' . str_repeat('0', $precision);
        $n_format = str_replace($dotzero, '', $n_format);
    }
    return $n_format . $suffix;
}

function apk($index)
{
    $ci = &get_instance();
    $ret = $ci->session->userdata("data_aplikasi")[$index];
    return $ret;
}

function user($index)
{
    $ci = &get_instance();
    $ret = $ci->session->userdata("user")[$index];
    return $ret;
}

function rupiah($angka)
{
    $hasil_rupiah = "Rp. " . number_format($angka, 0, ',', '.');
    return $hasil_rupiah;
}

// fungsi untuk ambil waktu yang lalu
function time_elapsed_string($datetime, $full = false)
{
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) {
        $string = array_slice($string, 0, 1);
    }

    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function statusTamu($status = 0)
{
    if ($status == "0") {
        $ret = 'Tidak Datang';
    } else {
        $ret = 'Datang';
    }
    return $ret;
}

function generateRandomString($length = 10, $lowercase = false, $alphabet = false)
{
    if ($lowercase && $alphabet) {
        $characters = 'abcdefghijklmnopqrstuvwxyz';
    } elseif ($lowercase) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    } else {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function str_replace_first($from, $to, $content)
{
    $from = '/' . preg_quote($from, '/') . '/';

    return preg_replace($from, $to, $content, 1);
}

// generate PDF
function generate_pdf($name, $tpl, $data, $assets = null)
{
    $ci = &get_instance();
    $data['data'] = $data;
    $ci->load->view($tpl, $data);
    // Get output html
    $html = $ci->output->get_output();
    // Load pdf library
    $ci->load->library('pdf');

    $ci->dompdf->loadHtml($html);
    // $ci->dompdf->options->setChroot(FCPATH);
    // setup size
    $ci->dompdf->setPaper('A4', 'landscape');
    // Render the HTML as PDF
    $ci->dompdf->render();
    // Output  PDF (1 = download and 0 = preview)
    $ci->dompdf->stream($name, array("Attachment" => 0));
    exit;
}

// konversi angka ke huruf untuk export excel
function number_to_alphabet($number)
{
    $number = intval($number);
    if ($number <= 0) {
        return '';
    }
    $alphabet = '';
    while ($number != 0) {
        $p = ($number - 1) % 26;
        $number = intval(($number - $p) / 26);
        $alphabet = chr(65 + $p) . $alphabet;
    }
    return $alphabet;
}
