<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	function ambil_data_session_aplikasi($par){
		$CI =& get_instance();
		return $CI->session->userdata('data_aplikasi')[$par];
	}
	
	function ambil_data_session_user($par){
		$CI =& get_instance();
		return $CI->session->userdata('user')[$par];
	}
	
	function cek_session(){
		$CI =& get_instance();
		if(empty($CI->session->userdata('user'))){
			redirect('Login/logout');
		}
	}
	
	function cek_session_frontend(){
		$CI =& get_instance();
		if(empty($CI->session->userdata('user'))){
			redirect('frontend/logout');
		}
	}
	
	function cek_session_user(){ // cek apakah user sedang login atau tidak
		$CI =& get_instance();
		if(!empty($CI->session->userdata('user'))){
			return true;
		}else{
			return false;
		}
	}
	
	function cek_user_penyedia(){ // cek apakah user sudah menjadi penyedia atau belum
		$CI =& get_instance();
		if($CI->session->userdata('user')['penyedia'] == "1" ){
			return true;
		}else{
			return false;
		}
	}
	
	function cek_level($level = "1"){
		$CI =& get_instance();
		if($CI->session->userdata('user')["level"] == $level){
			return true;
		}else{
			return false;
		}
	}
?>